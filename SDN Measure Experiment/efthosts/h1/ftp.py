from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer

authorizer = DummyAuthorizer()
authorizer.add_user('h3', 'passwdh3', './ftpServer/', perm='elradfmwMT')
authorizer.add_user('h4', 'passwdh4', './ftpServer/', perm='elradfmwMT')
authorizer.add_user('h5', 'passwdh5', './ftpServer/', perm='elradfmwMT')
authorizer.add_user('h6', 'passwdh6', './ftpServer/', perm='elradfmwMT')
authorizer.add_user('h7', 'passwdh7', './ftpServer/', perm='elradfmwMT')
authorizer.add_user('h8', 'passwdh8', './ftpServer/', perm='elradfmwMT')

handler = FTPHandler
handler.authorizer = authorizer

address = ('', 21)
server = FTPServer(address, handler)

server.max_cons = 256
server.max_cons_per_ip = 5

server.serve_forever()

