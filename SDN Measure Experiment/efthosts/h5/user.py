import random
import wget
import os
import time
from ftplib import FTP
import sys

idleProb = 24/25
action1Prob = (1 - idleProb)*0.6
action2Prob = (1 - idleProb)*0.2
action3Prob = (1 - idleProb)*0.2

def logprint(action, msg):
    currTime = time.strftime('%H:%M:%S',time.localtime(time.time()))
    print('[%s] (%s) Action%s: %s' % (currTime, usernm, action, msg))

def action1(host):
    if os.path.isfile('./httpDownload'):
        logprint(1, 'Cleaning httpdownload file')
        os.remove('./httpDownload')
    httpRequestShuffle = random.random()
    if httpRequestShuffle<0.5:
        url = 'http://' + host + '/index.html'
    elif httpRequestShuffle<0.8:
        url = 'http://' + host + '/fileS'
    elif httpRequestShuffle<0.9:
        url = 'http://' + host + '/fileM'
    else:
        url = 'http://' + host + '/fileL'
    logprint(1, 'HTTP downloading: ' + url)
    try:
        wget.download(url, 'httpDownload', bar=None)
        logprint(1, 'HTTP download complete: ' + url)
    except:
        logprint(1, 'HTTP download failed: ' + url)

def action2(host):
    if os.path.isfile('./ftpDownload'):
        logprint(2, 'Cleaning ftpdownload file')
        os.remove('./ftpDownload')
    logprint(2, 'Connecting to FTP server: ' + host)
    ftpRequestShuffle = random.random()
    if ftpRequestShuffle < 0.4:
        fileName = 'fileS'
    elif ftpRequestShuffle < 0.8:
        fileName = 'fileM'
    else:
        fileName = 'fileL'
    try:
        ftpObj = FTP()
        ftpObj.connect(host)
        ftpObj.login(usernm, 'passwd'+usernm)
        logprint(2, 'FTP downloading: "' + fileName + '" From: ' + host)
        ftpObj.retrbinary('RETR ' + fileName, open('./ftpDownload','wb').write)
        logprint(2, 'FTP download complete: "' + fileName + '" From: ' + host)
    except:
        logprint(2, 'FTP download failed: "' + fileName + '" From: ' + host)
    
def action3(host):
    logprint(3, 'Connecting to FTP server: ' + host)
    ftpRequestShuffle = random.random()
    if ftpRequestShuffle<0.4:
        fileName = 'fileS'
    elif ftpRequestShuffle<0.8:
        fileName = 'fileM'
    else:
        fileName = 'fileL'
    try:
        ftpObj = FTP()
        ftpObj.connect(host)
        ftpObj.login(usernm, 'passwd'+usernm)
        if usernm+'file' in ftpObj.nlst():
            rtnmsg = ftpObj.delete(usernm+'file')
            logprint(3, 'Cleaning remote file')
        logprint(3, 'FTP uploading: "' + fileName + '" To: ' + host)
        ftpObj.storbinary("STOR "+usernm+'file', open(fileName, 'rb'))
        logprint(3, 'FTP upload complete: "' + fileName + '" To: ' + host)
    except:
        logprint(3, 'FTP upload failed: "' + fileName + '" To: ' + host)
        
if __name__ == '__main__':
    if len(sys.argv) == 2:
        usernm = sys.argv[1]
        while True:
            actionShuffle = random.random()
            host = random.choice(['10.0.0.101', '10.0.0.102'])
            if actionShuffle < action1Prob:
                action1(host)
            elif actionShuffle < action1Prob + action2Prob:
                action2(host)
            elif actionShuffle < action1Prob + action2Prob + action3Prob:
                action3(host)
            else:
                pass
            time.sleep(1)
