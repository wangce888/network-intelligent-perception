#!/usr/bin/env python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call

import threading

def myNetwork():

    net = Mininet( topo=None,
                   build=False,
                   ipBase='10.0.0.0/8')

    info( '*** Adding controller\n' )
    c0=net.addController(name='c0',
                      controller=RemoteController,
                      ip='127.0.0.1',
                      protocol='tcp',
                      port=6633)

    info( '*** Add switches\n')
    s1 = net.addSwitch('s1', cls=OVSKernelSwitch)
    s2 = net.addSwitch('s2', cls=OVSKernelSwitch)
    s3 = net.addSwitch('s3', cls=OVSKernelSwitch)
    s4 = net.addSwitch('s4', cls=OVSKernelSwitch)

    info( '*** Add hosts\n')
    h1 = net.addHost('h1', cls=Host, ip='10.0.0.101', defaultRoute=None, mac='00:00:00:00:0A:01')
    h2 = net.addHost('h2', cls=Host, ip='10.0.0.102', defaultRoute=None, mac='00:00:00:00:0A:02')
    h3 = net.addHost('h3', cls=Host, ip='10.0.0.201', defaultRoute=None, mac='00:00:00:00:0B:01')
    h4 = net.addHost('h4', cls=Host, ip='10.0.0.202', defaultRoute=None, mac='00:00:00:00:0B:02')
    h5 = net.addHost('h5', cls=Host, ip='10.0.0.203', defaultRoute=None, mac='00:00:00:00:0B:03')
    h6 = net.addHost('h6', cls=Host, ip='10.0.0.211', defaultRoute=None, mac='00:00:00:00:0C:01')
    h7 = net.addHost('h7', cls=Host, ip='10.0.0.212', defaultRoute=None, mac='00:00:00:00:0C:02')
    h8 = net.addHost('h8', cls=Host, ip='10.0.0.213', defaultRoute=None, mac='00:00:00:00:0C:03')

    info( '*** Add links\n')
    net.addLink(s1, s2)
    net.addLink(s1, s3, cls=TCLink, loss=5)
    #net.addLink(s1, s3)
    net.addLink(s1, s4)
    net.addLink(s2, h1)
    net.addLink(s2, h2)
    net.addLink(s3, h3)
    net.addLink(s3, h4)
    net.addLink(s3, h5)
    net.addLink(s4, h6)
    net.addLink(s4, h7)
    net.addLink(s4, h8)

    info( '*** Starting network\n')
    net.build()
    info( '*** Starting controllers\n')
    for controller in net.controllers:
        controller.start()

    info( '*** Starting switches\n')
    net.get('s1').start([c0])
    net.get('s2').start([c0])
    net.get('s3').start([c0])
    net.get('s4').start([c0])

    info( '*** Post configure switches and hosts\n')
    s1.cmd('ifconfig s1 10.0.0.11')
    s2.cmd('ifconfig s2 10.0.0.21')
    s3.cmd('ifconfig s3 10.0.0.22')
    s4.cmd('ifconfig s4 10.0.0.23')

    info('*** Pingall & initialize flow table\n')
    #net.pingAll()    
    
    info( '*** Deploying Services\n')
    h1.cmd('cd efthosts/h1')
    h1.cmd('python -m http.server 80 -d httpServer/ &')
    h1.cmd('python ftp.py &')
    
    h2.cmd('cd efthosts/h2')
    h2.cmd('python -m http.server 80 -d httpServer/ &')
    h2.cmd('python ftp.py &')
    
    h3.cmd('cd efthosts/h3')
    h4.cmd('cd efthosts/h4')
    h5.cmd('cd efthosts/h5')
    h6.cmd('cd efthosts/h6')
    h7.cmd('cd efthosts/h7')
    h8.cmd('cd efthosts/h8')
    
    # threading.Thread(target=h3.cmdPrint, args=('python user.py h3',)).start()
    # threading.Thread(target=h4.cmdPrint, args=('python user.py h4',)).start()
    # threading.Thread(target=h5.cmdPrint, args=('python user.py h5',)).start()
    # threading.Thread(target=h6.cmdPrint, args=('python user.py h6',)).start()
    # threading.Thread(target=h7.cmdPrint, args=('python user.py h7',)).start()
    # threading.Thread(target=h8.cmdPrint, args=('python user.py h8',)).start()

    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()

