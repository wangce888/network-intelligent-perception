#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call

def myNetwork():

    net = Mininet( topo=None,
                   build=False,
                   ipBase='172.16.0.0/24')

    info( '*** Adding controller\n' )
    c0 = net.addController(name='c0', controller=RemoteController, ip='172.16.0.5', protocol='tcp', port=6653)
    #controller = net.addController('RemoteController', controller=RemoteController, ip='172.16.0.5', port=6653)

    info( '*** Add switches\n')
    sw1 = net.addSwitch('sw1', cls=OVSKernelSwitch, dpid='5e3e486e7302044c')
    sw2 = net.addSwitch('sw2', cls=OVSKernelSwitch, dpid='5e3e486e7302034a')
    sw3 = net.addSwitch('sw3', cls=OVSKernelSwitch, dpid='5e3e486e73020335')

    info( '*** Add hosts\n')
    h1 = net.addHost('h1', cls=Host, ip='172.16.0.1', mac='B8:AE:ED:22:1D:4B', defaultRoute=None)
    attacker = net.addHost('attacker', cls=Host, ip='172.16.0.6', defaultRoute=None)
    s1 = net.addHost('s1', cls=Host, ip='172.16.0.2', mac='20:47:47:72:75:05', defaultRoute=None)
    s2 = net.addHost('s2', cls=Host, ip='172.16.0.3', mac='B8:AE:ED:22:1D:73', defaultRoute=None)
    s3 = net.addHost('s3', cls=Host, ip='172.16.0.4', mac='B8:AE:ED:22:19:EE', defaultRoute=None)

    info( '*** Add links\n')
    net.addLink(sw1, h1, port1=3, port2=1)
    net.addLink(sw1, attacker, port1=5, port2=1)
    net.addLink(sw1, sw2, port1=2, port2=2)
    net.addLink(sw1, sw3, port1=1, port2=1)
    net.addLink(sw2, s1, port1=4, port2=1)
    net.addLink(sw2, sw3, port1=3, port2=3)
    net.addLink(sw3, s2, port1=4, port2=1)
    net.addLink(sw3, s3, port1=2, port2=1)

    info( '*** Starting network\n')
    net.build()
    info( '*** Starting controllers\n')
    for controller in net.controllers:
		controller.start()


    info( '*** Starting switches\n')
    net.get('sw1').start([c0])
    net.get('sw2').start([c0])
    net.get('sw3').start([c0])

    info( '*** Post configure switches and hosts\n')

    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()