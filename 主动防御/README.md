# 主动防御

#### 介绍
本项目为基于SDN的网络攻击移动目标防御方法。区别于传统的静态化、补丁式的防御模式，该方法借助软件定义网络（Software Defined Network, SDN）的开放性与灵活性，部署移动目标防御中的IP地址跳变、端口跳变、路径变换、网络接口变换以及协议号/TCP选项的动态变更等，从而能够持续不断地动态转移当前被保护网络系统的网络攻击面信息，致使攻击者在前期侦察探测过程中获取的信息失效，并误导攻击者攻击无效或者错误的网络目标，由此达到消耗攻击者攻击资源、扭转网络安全“易攻难守”局面的核心目标，从而高效保护目标网络系统。

该方法的实验环境与工作流程如下图所示：

<div align=center>
<img width="600" height="450" src="https://gitee.com/network-intelligent-perception/active-defense/raw/master/Figures/Topology.jpg">
</div>

整个防御方法的实现流程主要分为以下5个步骤：

1.  移动目标防御系统初始化，启动网络层与主机层面的安全威胁事件检测
2.  由攻击测试主机运行模拟攻击脚本，实施网络层/主机层攻击
3.  部署有威胁感知服务的主机或服务器（如图中S1,S2）收集态势信息，并向决策处理服务器（图中S3）上报
4.  将威胁事件存入后台数据库，经数据关联分析后，制定防御策略并下发至SDN控制
5.  SDN控制器运行移动目标防御应用，触发网络层移动目标防御机制，完成防御

#### 软件架构

本防御方法总共包括四个模块：

1.  实验环境拓扑 （位于Mininet文件夹）
2.  SDN控制器模块 （位于Controller文件夹）
3.  模拟攻击模块 （位于Attacker文件夹）
4.  威胁采集与预处理模块 （位于S1、S2文件夹）
5.  威胁分析与决策模块 （位于S3文件夹）

#### 安装教程

| 序号 | 软件名称及版本               | 备注                           |
|----|-----------------------|------------------------------|
| 1  | POX控制器，版本eel          | SDN架构的控制器服务                  |
| 2  | MySQL数据库，版本14.14      | 实验环境所需的数据库服务                 |
| 3  | QEMU emulator，版本2.5.0 | 实验环境中的虚拟化服务以及操作系统层的攻击面参数变换功能 |
| 4  | Nmap攻击工具，版本7.60       | 实验环境中的攻击测试服务                 |
| 5  | Mininet仿真工具，版本2.3.0   | 实验环境中的网络拓扑构建                             |

#### 使用说明

1.  运行Mininet文件夹下的topo-mtd.py文件与Controller文件夹下的startPOX4Mininet.sh文件
2.  运行S1文件夹下的moniotr.sh,vm.sh以及watch.sh文件，运行S2文件夹下的read_filesystem.py,read_host.py以及watch.sh文件
3.  运行S3文件夹下的decision.py文件
4.  运行Attacker文件夹下的nmp.py攻击测试文件

#### 预期结果

本方法在实施网络层移动目标防御变换后，将达到如下图的示例结果：

<div align=center>
<img width="800" height="450" src="https://gitee.com/network-intelligent-perception/active-defense/raw/master/Figures/Result.jpg">
</div>

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
