#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Description:JSON文件解析后的决策制定(缺省网络和OS操作情况下的最完善版本)
Author:yyzhou
Updated at 2019.07.16
"""

import json
import os
import socket
import struct
import random
import time
import MySQLdb
import random
import tcpclient_sendcmd


# 判断消息类别
def msg(d):
    if d['msg'] == 'report':
        return 0
    else:
        return 1


# 判断消息层次(某些层只看after的上报信息)
def layer(d):
    if d['layer'] == 'data':
        return 0
    elif d['layer'] == 'software' and d['time'] == 'after':
        return 1
    elif d['layer'] == 'runtime':
        return 2
    elif d['layer'] == 'os' and d['time'] == 'after':
        return 3
    elif d['layer'] == 'network' and d['time'] == 'after':
        return 4
    elif d['layer'] == 'attack':
        return 5
    elif d['layer'] == 'performance':
        return 6
    else:
    	return 7


# # 数据层处理函数
# # 赋值，后期处理备用
# def function_data(d):
#     filename = []
#     rwaccess = []
#     key = d['layer_info']['random']['key']
#     randomkey = d['layer_info']['random']['key']
#     original = d['layer_info']['encrypt']['original']
#     encrypted = d['layer_info']['encrypt']['encrypted']
#     # 循环遍历数据层JSON中的COPIES对象
#     for i in range(len(d['layer_info']['copies'])):
#         # 未知长度的列表不能直接赋值，而要采取append添加进列表
#         filename.append(d['layer_info']['copies'][i]['filename'])
#         rwaccess.append(d['layer_info']['copies'][i]['rwaccess'])
#
#     return key, randomkey, original, encrypted, filename, rwaccess


# 软件层处理函数
# 赋值，后期处理备用
def function_software(d):
    pdf = d['layer_info']['type']['pdf']
    browser = d['layer_info']['type']['browser']
    php = d['layer_info']['version']['php']
    java = d['layer_info']['version']['java']

    return pdf, browser, php, java


# # Runtime层处理函数
# # 赋值，后期处理备用
# def function_runtime(d):
#     text_addr = d['layer_info']['ASLR']['text_addr']
#     bss_addr = d['layer_info']['ASLR']['bss_addr']
#     data_addr = d['layer_info']['ASLR']['data_addr']
#     stack_addr = d['layer_info']['ASLR']['stack_addr']
#
#     isr_stack_addr = d['layer_info']['ISR']['stack_addr']
#     heap1kb_addr = d['layer_info']['ISR']['heap1kb_addr']
#     heap100kb_addr = d['layer_info']['ISR']['heap1kb_addr']
#     heap1mb_addr = d['layer_info']['ISR']['heap1kb_addr']
#
#     return text_addr, bss_addr, data_addr, stack_addr,\
#         isr_stack_addr, heap1kb_addr, heap100kb_addr, heap1mb_addr


# OS层处理函数
# 赋值，后期处理备用
def function_os(d):
    vmname = []
    ostype = []
    osversion = []
    osbit = []
    state = []
    host = d['layer_info']['host']
    # 循环遍历OS层JSON中的VMs对象
    for i in range(len(d['layer_info']['VMs'])):
        vmname.append(d['layer_info']['VMs'][i]['vmname'])
        ostype.append(d['layer_info']['VMs'][i]['ostype'])
        osversion.append(d['layer_info']['VMs'][i]['osversion'])
        osbit.append(d['layer_info']['VMs'][i]['osbit'])
        state.append(d['layer_info']['VMs'][i]['state'])

    return vmname, ostype, osversion, osbit, state, host


# 网络层处理函数
# 赋值，后期处理备用
def function_network(d):
    hostname = []
    ip = []
    ser_port = []
    vip = []
    vport = []
    prtcode = []
    tcpoption = []
    route1 = []
    route2 = []
    route3 = []
    # 循环遍历网络层JSON中的routes对象
    for i in range(len(d['layer_info'])):
        hostname.append(d['layer_info'][i]['hostname'])
        ip.append(d['layer_info'][i]['ip'])
        ser_port.append(d['layer_info'][i]['ser_port'])
        vip.append(d['layer_info'][i]['vip'])
        vport.append(d['layer_info'][i]['vport'])
        prtcode.append(d['layer_info'][i]['prtcode'])
        tcpoption.append(d['layer_info'][i]['tcpoption'])

    for j in range(len(d['routes_info']['route1'])):
        route1.append(d['routes_info']['route1'][j])

    for k in range(len(d['routes_info']['route2'])):
        route2.append(d['routes_info']['route2'][k])

    for l in range(len(d['routes_info']['route3'])):
        route3.append(d['routes_info']['route3'][l])

    return hostname, ip, ser_port, vip, vport, prtcode, tcpoption, route1, route2, route3


# 威胁信息处理函数
# 赋值，后期处理备用
def function_attack(d):
	targetlayer = []
	risk = d['risk']
	attacktype = d['layer_info']['attacktype']
	targethost = d['layer_info']['target']['targethost']

	# 循环遍历数据层JSON中的targetlayer对象
	for i in range(len(d['layer_info']['targetlayer'])):
		targetlayer.append(d['layer_info']['targetlayer'][i])

	detail = d['layer_info']['target']['detail']
	return risk, attacktype, targethost, targetlayer, detail


# 性能信息处理函数
# 赋值，后期处理备用
def function_performance(d):
    host = d['layer_info']['host']
    CPU_uti = float(d['layer_info']['CPU_uti'].strip('%'))
    RAM_uti = float(d['layer_info']['RAM_uti'].strip('%'))  # delete % and convert to float
    return host, CPU_uti, RAM_uti


# runtime层命令JSON生成函数(含参)
def runtime_cmd(p):
    runtime_dir = {
        "msg": "transfercmd",
        "dsthost": "S2",
        "layer": "runtime",
        "class": "ASLR&ISR",
        "transfer_info": p
    }
    path = os.getcwd() + '/JsonFileCmd/'  # 获取当前工程下的JsonFile目录
    with open(path + 'runtime_transfercmd.json', "w") as f:
        json.dump(runtime_dir, f)
        print("runtime层命令写入json成功#######################")
    f.close()
    return 0


# OS层命令JSON生成函数(含参)
# h表示主机号，p=0表示类型、位数，p=1表示类型、版本，p=2表示版本、位数，p=3表示全转移
def os_cmd(h, p):
    x = ''
    y = ''
    if h == 'S1':
        for i in range(len(state1)):
            if state1[i] == 'ON':
                x = vmname1[i]
                for j in range(len(vmname1)):
                    if p == 0:
                        if (ostype1[j] != ostype1[i]) and (osbit1[j] != osbit1[i]):
                            y = vmname1[j]
                    elif p == 1:
                        if (ostype1[j] != ostype1[i]) and (osbit1[j] == osbit1[i]) and (osversion1[j] != osversion1[i]):
                            y = vmname1[j]
                    elif p == 2:
                        if (ostype1[j] is ostype1[i]) and (osbit1[j] != osbit1[i]) and (osversion1[j] != osversion1[i]):
                            y = vmname1[j]
                    elif p == 3:
                        if (ostype1[j] != ostype1[i]) and (osbit1[j] != osbit1[i]) and (osversion1[j] != osversion1[i]):
                            y = vmname1[j]

    elif h == 'S2':
        for i in range(len(state2)):
            if state2[i] == 'ON':
                x = vmname2[i]
                for j in range(len(vmname2)):
                    if p == 0:
                        if (ostype2[j] != ostype2[i]) and (osbit2[j] != osbit2[i]):
                            y = vmname2[j]
                    elif p == 1:
                        if (ostype2[j] != ostype2[i]) and (osbit2[j] == osbit2[i]) and (osversion2[j] != osversion2[i]):
                            y = vmname2[j]
                    elif p == 2:
                        if (ostype2[j] is ostype2[i]) and (osbit2[j] != osbit2[i]) and (osversion2[j] != osversion2[i]):
                            y = vmname2[j]
                    elif p == 3:
                        if (ostype2[j] != ostype2[i]) and (osbit2[j] != osbit2[i]) and (osversion2[j] != osversion2[i]):
                            y = vmname2[j]

    os_dir = {
        "msg": "transfercmd",
        "dsthost": h,
        "layer": "os",
        "class": "type",
        "transfer_info": [
            {
                "vmname": x,
                "operate": "OFF"
            },
            {
                "vmname": y,
                "operate": "ON"
            }
        ]
    }
    path = os.getcwd() + '/JsonFileCmd/'  # 获取当前工程下的JsonFile目录
    with open(path + 'os_transfercmd_' + h + '.json', "w") as f:
        json.dump(os_dir, f)
        print("os层命令写入json成功#######################")
    f.close()
    return 0


# software层命令JSON生成函数(含参)
# p1表示类型或版本，p2表示具体的软件
def software_cmd(p1, p2):
    v = ''
    if p1 == 'version':
        if p2 == 'java':
            if java == '12.0.2':
                v = '1.8'
            else:
                v = '1.12'
        elif p2 == 'php':
            if php == '7.0.33-12':
                v = '5.6'
            else:
                v = '7.0'
    elif p1 == 'type':
        if p2 == 'pdf':
            if pdf == 'FoxitReader':
                v = 'evince'
            else:
                v = 'FoxitReader'
        elif p2 == 'browser':
            if browser == 'firefox':
                v = 'google-chrome'
            else:
                v = 'firefox'

    software_dir = {
        "msg": "transfercmd",
        "dsthost": "S2",
        "layer": "software",
        "class": p1,
        "transfer_info":
            {
                "software": p2,
                "value": v
            }
    }
    path = os.getcwd() + '/JsonFileCmd/'  # 获取当前工程下的JsonFile目录
    with open(path + 'software_transfercmd_' + p2 + '.json', "w") as f:
        json.dump(software_dir, f)
        print("software层命令写入json成功#######################")
    f.close()
    return 0


# data层命令JSON生成函数(含参)
def data_cmd(p_data):
    data_dir = {
        "msg": "transfercmd",
        "dsthost": "S2",
        "layer": "data",
        "class": p_data,
        "transfer_info": 1
    }
    path = os.getcwd() + '/JsonFileCmd/'  # 获取当前工程下的JsonFile目录
    with open(path + 'data_transfercmd_' + p_data + '.json', "w") as f:
        json.dump(data_dir, f)
        print("data层命令写入json成功#######################")
    f.close()
    return 0


# P1_NET表示哪一台主机
# p2_net = 0表示IP地址，为1表示tcp选项
def network_cmd(p1_net, p2_net):
    v = ''
    a = ''
    if p2_net == 0:
        a = 'ipaddr'
        if p1_net == 'S1':
            int_ip1 = random.randint(2886729738, 2886729856)
            v = socket.inet_ntoa(struct.pack('I', socket.htonl(int_ip1)))
        elif p1_net == 'S2':
            int_ip2 = random.randint(2886729857, 2886729982)
            v = socket.inet_ntoa(struct.pack('I', socket.htonl(int_ip2)))
    elif p2_net == 1:
        a = 'tcpoption'
        seed = random.randint(268435456, 4294967295)
        v = hex(seed)

    network_dir = {
        "msg": "transfercmd",
        "dsthost": p1_net,
        "layer": "network",
        "class": a,
        "transfer_info": v
    }
    path = os.getcwd() + '/JsonFileCmd/'  # 获取当前工程下的JsonFile目录
    with open(path + 'network_transfercmd_' + p1_net + '_' + a + '.json', "w") as f:
        json.dump(network_dir, f)
        print("网络层命令写入json成功#######################")
    f.close()
    return 0


# p1_port表示哪个主机，p2_port为0表示端口，2表示协议代码
def port_cmd(p1_port, p2_port):
    v = 0
    a = ''
    if p2_port == 0:
        a = 'port'
        v = random.randint(5001, 65535)
    elif p2_port == 2:
        a = 'prtcode'
        v = random.randint(1, 132)

    port_dir = {
        "msg": "transfercmd",
        "dsthost": p1_port,
        "layer": "network",
        "class": a,
        "transfer_info": v
    }
    path = os.getcwd() + '/JsonFileCmd/'  # 获取当前工程下的JsonFile目录
    with open(path + 'port_transfercmd_' + p1_port + '_' + a + '.json', "w") as f:
        json.dump(port_dir, f)
        print("Port相关命令写入json成功#######################")
    f.close()
    return 0


# p_route为"route1"表示route1，"route2"表示route2，"route3"表示route3
def routes_cmd(p_route):
    v = []
    if p_route == 'route1':
        if route1[3] == 'S2':
            v = ["S1", "OSW2", "OSW1", "OSW3", "S2"]
        else:
            v = ["S1", "OSW2", "OSW3", "S2"]
    elif p_route == 'route2':
        if route2[3] == 'H1':
            v = ["S1", "OSW2", "OSW3", "OSW1", "H1"]
        else:
            v = ["S1", "OSW2", "OSW1", "H1"]
    elif p_route == 'route3':
        if route3[3] == 'H1':
            v = ["S2", "OSW3", "OSW2", "OSW1", "H1"]
        else:
            v = ["S2", "OSW3", "OSW1", "H1"]

    routes_dir = {
        "msg": "transfercmd",
        "dsthost": "C1",
        "layer": "network",
        "class": "routes",
        "route": p_route,
        "transfer_info": v
    }
    path = os.getcwd() + '/JsonFileCmd/'  # 获取当前工程下的JsonFile目录
    with open(path + 'routes_transfercmd_' + p_route + '.json', "w") as f:
        json.dump(routes_dir, f)
        print("Routes命令写入json成功#######################")
    f.close()
    return 0


# 网络接口（网卡）切换控制命令
def interface_cmd(p):
    interface_dir = {
        "msg": "transfercmd",
        "dsthost": p,
        "layer": "network",
        "class": "interface"
    }
    path = os.getcwd() + '/JsonFileCmd/'  # 获取当前工程下的JsonFile目录
    with open(path + 'interface_transfercmd_' + p + '.json', "w") as f:
        json.dump(interface_dir, f)
        print("Interface命令写入json成功#######################")
    f.close()
    return 0


# 威胁攻击信息随机风险函数
def random_risk(v):
    value = v + 0.001 * random.randint(1, 140)
    return value


# 威胁信息处理函数
def function_risk(d):
    rank = d['risk']
    x = [-1, 0, 1]
    quantity = (2*(len(d['layer_info']['targetlayer']))+random.choice(x))
    type = d['layer_info']['attacktype']
    target = d['layer_info']['target']['targethost']
    layer = [str(i) for i in d['layer_info']['targetlayer']]
    layer = ', '.join(layer)
    index = (3*(len(d['layer_info']['targetlayer']))+random.choice(x))
    return rank, quantity, type, target, layer, index


# 威胁攻击信息风险评估函数(重点)
def calculate_risk(d):
    if d['risk'] == 'low':
        value = 0.25 + 0.001 * random.randint(0, 9)
    elif d['risk'] == 'middle':
        value = 0.5 + 0.001 * random.randint(0, 9)
    elif d['risk'] == 'high':
        value = 0.75 + 0.001 * random.randint(0, 9)

    if d['layer_info']['attacktype'] == 'unknown':
        value = value + 0.04 + 0.001 * random.randint(0, 9)

    if len(d['layer_info']['targetlayer']) == 0:
        value = value + 0.001 * random.randint(0, 9)
    elif len(d['layer_info']['targetlayer']) == 1:
        value = value + 0.006 * random.randint(0, 10)
    elif len(d['layer_info']['targetlayer']) == 2:
        value = value + 0.008 * random.randint(0, 10)
    elif len(d['layer_info']['targetlayer']) == 3:
        value = value + 0.01 * random.randint(0, 10)
    else:
        value = value + 0.012 * random.randint(0, 10)

    value = "%.3f" % value

    return value


if __name__ == '__main__':
    t = 0
    pdf = ''
    browser = ''
    php = ''
    java = ''
    attacktype = ''
    state1 = []
    state2 = []
    vmware1 = []
    vmware2 = []
    route1 = []
    route2 = []
    route3 = []
    flag = 0
    # time.sleep(15)
    # t = random.randint(8, 18)
    # t = t + 0.1 * random.randint(0, 9)
    # time.sleep(t)
    
    conn = MySQLdb.connect(
        host='localhost',
        port=3306,
        user='root',
        passwd='cnv6201',
        db='PRESENTATION',
        charset='utf8',
    )
    

    while True:
        # 每次循环开始时，遍历当前目录下所有Report JSON文件，读取当前各攻击面情况
        path = os.getcwd() + '/JsonFileReport/'  # 获取到JsonFileReport目录
        dirs = os.listdir(path)
        for i in dirs:  # 循环读取路径下的文件
            if os.path.splitext(i)[1] == ".json":  # 筛选JSON文件
                with open(path + i, 'r', encoding='UTF-8') as f:
                	data = json.load(f)
                	# 判断消息类型是否为上报
                	if msg(data) == 0:
                		# 判断消息层次,用多个变量分别直接接收相应层函数的返回值（序列解包
                		if data['layer'] == 'software':
                			pdf, browser, php, java = function_software(data)
                		elif data['layer'] == 'os' and data['time'] == 'after':
                			if data['layer_info']['host'] == 'S1' and data['time'] == 'after':
                				vmname1, ostype1, osversion1, osbit1, state1, host1 = function_os(data)
                			if data['layer_info']['host'] == 'S2' and data['time'] == 'after':
                				vmname2, ostype2, osversion2, osbit2, state2, host2 = function_os(data)
                		elif data['layer'] == 'network' and data['time'] == 'after':
                			hostname, ip, ser_port, vip, vport, prtcode, tcpoption, \
                			route1, route2, route3 = function_network(data)
                	else:
                		print("数据不是上报类型，不归我管！")
                f.close()

        # path = os.getcwd() + '/JsonPerformanceReport/'  # 获取到PerformanceReport目录
        # dirs = os.listdir(path)
        # for i in dirs:  # 循环读取路径下的文件
        #     if os.path.splitext(i)[1] == ".json":  # 筛选JSON文件
        #         with open(path + i, 'r', encoding='UTF-8') as f:
        #             data = json.load(f)
        #             # 判断消息类型是否为上报
        #             if msg(data) == 0:
        #                 # 判断消息层次,用多个变量分别直接接收相应层函数的返回值（序列解包）
        #                 if layer(data) == 6:
        #                     if data['layer_info']['host'] == 'S1':
        #                         host1, CPU_uti1, RAM_uti1 = function_performance(data)
        #                     elif data['layer_info']['host'] == 'S2':
        #                         host2, CPU_uti2, RAM_uti2 = function_performance(data)
        #                     elif data['layer_info']['host'] == 'C1':
        #                         host3, CPU_uti3, RAM_uti3 = function_performance(data)
        #             else:
        #                 print("数据不是上报类型，不归我管！")
        #         f.close()

        # 判断攻击事件的文件夹下是否存在文件
        path = os.getcwd() + '/AttackReport/'  # 获取到AttackReport目录
        if not os.listdir(path):  # AttackReport目录为空，此时无攻击
            if flag == 0:  # 无攻击或一轮攻击结束
                print("目录为空")
                print("无威胁时清除展示界面的相关信息（风险等级，影响层面等）")
                # 无威胁时候计算风险系数，其余为“暂无”，做展示用
                risk_value = 0.1 + 0.001 * random.randint(1, 9)
                risk_value_end = "%.3f" % random_risk(risk_value)
                coefficient = str(risk_value_end)
                rank = 'low'
                quantity = '0'
                type = 'None'
                target = 'None'
                layer = 'None'
                index = 'None'
                # print(coefficient, rank, quantity, type, target, layer, index)
                cur = conn.cursor()
                cur.execute("delete from presentation_threat_level")
                cur.execute(
                    "insert into presentation_threat_level(`coefficient`, `rank`, `quantity`, `type`, `target`, `layer`, `index`) "
                    "values(%s,%s,%s,%s,%s,%s,%s)",
                    [coefficient, rank, quantity, type, target, layer, index])
                cur.close()
                conn.commit()
            # 以下摘自原situation.py代码
            # cur.execute(
            #     "insert into presentation_threat_level(`coefficient`, `rank`, `quantity`, `type`, `target`, `layer`, `index`) "
            #     "values(%s,%s,%s,%s,%s,%s,%s)",
            #     [coefficient, rank, quantity, type, target, layer, index])
            # print("done")

            # 定时随机变换 T=20s
                v = random.randint(0, 2)
                runtime_cmd(v)
                list = ['route1', 'route2', 'route3']
                routes_cmd(list[v])
                path = os.getcwd() + '/JsonFileCmd/'  # 获取当前工程下的JsonFileCmd目录
                if not os.listdir(path):  # JsonFileCmd目录为空
                    pass
                else:
                    tcpclient_sendcmd.send()
                    time.sleep(15)
                    dirs = os.listdir(path)
                    for i in dirs:  # 循环读取路径下的文件
                        os.remove(path + i)  # 删除JSON文件
                time.sleep(15)
            continue  # 继续下个循环
        else:
            dirs = os.listdir(path)
            for i in dirs:  # 循环读取路径下的文件
                if os.path.splitext(i)[1] == ".json":  # 筛选JSON文件
                    with open(path + i, 'r', encoding='UTF-8') as f:
                        data = json.load(f)
                        # 判断消息类型是否为上报
                        if msg(data) == 0:
                            # 判断消息层次,用多个变量分别直接接收相应层函数的返回值（序列解包）
                            if data['layer'] == 'attack':
                                risk, attacktype, targethost, targetlayer, detail = function_attack(data)
                                rank, quantity, type, target, layer, index = function_risk(data)
                                coefficient = calculate_risk(data)
                                cur = conn.cursor()
                                cur.execute("delete from presentation_threat_level")
                                cur.execute(
                                    "insert into presentation_threat_level(`coefficient`, `rank`, `quantity`, `type`, `target`, `layer`, `index`) "
                                    "values(%s,%s,%s,%s,%s,%s,%s)",
                                    [coefficient, rank, quantity, type, target, layer, index])
                                cur.close()
                                conn.commit()
                                # 判断是否是一次新的攻击开始，新的攻击开始时清除威胁展示界面展示的流程，清除决策，清除决策时间
                                if detail == 'start':  # 两种情况（全网段扫描，或者第一个ssh连接建立）
                                    flag = 1
                                    # print("清除威胁页面展示的上次攻击的步骤，影响的层面等信息")
                                    # print("清除决策页面的上一次攻击的决策（包括清除展示的决策时间）")
                                    t = 0  # 清除本地的决策时间缓存
                                    
                                    # # 清空决策数据表
                                    cur = conn.cursor()
                                    cur.execute("delete from presentation_attack_step")
                                    cur.execute("delete from presentation_data_decision")
                                    cur.execute("delete from presentation_runtime_decision")
                                    cur.execute("delete from presentation_software_decision")
                                    cur.execute("delete from presentation_os_decision")
                                    cur.execute("delete from presentation_ip_decision")
                                    cur.execute("delete from presentation_decision_time")
                                    cur.close()
                                    conn.commit()
                                    
                                    if attacktype == 'ssh_remote_connection':
                                        if targethost == 'S1' or targethost == 's1':  # 兼容大小写，防止错误
                                            # print("攻击者对S1建立了SSH连接")  # 展示到威胁感知界面上的攻击步骤中
                                            step = "攻击者对S1建立了SSH连接"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                            cur.close()
                                            conn.commit()
                                            # 开始决策
                                            time_start = time.time()
                                            t = random.randint(8, 18)
                                            t = t + 0.1 * random.randint(0, 9)
                                            time.sleep(t)
                                            network_cmd("S1", 1)
                                            port_cmd("S1", 2)
                                            v = random.randint(0, 2)
                                            runtime_cmd(v)
                                            # print("变换S1的TCP选项与协议代码")  # 决策内容
                                            
                                            i_decision = "变换S1的TCP选项与协议代码"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_ip_decision(i_decision) values(%s)", [i_decision])
                                            cur.close()
                                            conn.commit()
                                            time_end = time.time()
                                            t = time_end - time_start
                                        elif targethost == 'S2' or targethost == 's2':
                                            # print("攻击者对S2建立了SSH连接")  # 展示到威胁感知界面上的攻击步骤中
                                            step = "攻击者对S2建立了SSH连接"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                            cur.close()
                                            conn.commit()
                                            # 开始决策
                                            time_start = time.time()
                                            t = random.randint(8, 18)
                                            t = t + 0.1 * random.randint(0, 9)
                                            time.sleep(t)
                                            network_cmd("S2", 1)
                                            port_cmd("S2", 2)
                                            v = random.randint(0, 2)
                                            runtime_cmd(v)
                                            # print("变换S2的TCP选项与协议代码")  # 决策内容
                                            
                                            i_decision = "变换S2的TCP选项与协议代码"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_ip_decision(i_decision) values(%s)", [i_decision])
                                            cur.close()
                                            conn.commit()
                                            time_end = time.time()
                                            t = time_end - time_start
                                    elif attacktype == 'all_ip_scan':  # 攻击者扫描整个网段，攻击类型的名称待定
                                        # print("攻击者对整个网段内的IP地址进行扫描")  # 展示到威胁感知界面上的攻击步骤中  攻击事件展示
                                        step = "攻击者对整个网段内的IP地址进行扫描"
                                        cur = conn.cursor()
                                        cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                        cur.close()
                                        conn.commit()
                                        
                                        time_start = time.time()
                                        t = random.randint(8, 18)
                                        t = t + 0.1 * random.randint(0, 9)
                                        time.sleep(t)
                                        routes_cmd("route1")
                                        routes_cmd("route2")
                                        routes_cmd("route3")
                                        # print("变换整体网络路径")  # 决策内容
                                        
                                        i_decision = "变换整体网络路径"
                                        cur = conn.cursor()
                                        cur.execute("insert into presentation_ip_decision(i_decision) values(%s)", [i_decision])
                                        cur.close()
                                        conn.commit()
                                        time_end = time.time()
                                        t = time_end - time_start
                                elif detail == 'end':  # 两种情况（DoS，或者攻击者断开ssh连接）
                                    flag = 0
                                    if attacktype == 'ssh_remote_connection':
                                        if targethost == 'S1' or targethost == 's1':  # 兼容大小写，防止错误
                                            # print("攻击者从S1上断开了SSH连接")  # 展示到威胁感知界面上的攻击步骤中
                                            step = "攻击者从S1上断开了SSH连接"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                            cur.close()
                                            conn.commit()
                                        elif targethost == 'S2' or targethost == 's2':
                                            # print("攻击者从S2上断开了SSH连接")  # 展示到威胁感知界面上的攻击步骤中
                                            step = "攻击者从S2上断开了SSH连接"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                            cur.close()
                                            conn.commit()
                                        # 无决策，只需输出前序的决策时间到页面
                                    elif attacktype == 'dos':
                                        if targethost == 'S1' or targethost == 's1':  # 兼容大小写，防止错误
                                            # print("攻击者对S1发动了DoS攻击")  # 展示到威胁感知界面上的攻击步骤中
                                            step = "攻击者对S1发动了DoS攻击"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                            cur.close()
                                            conn.commit()
                                            # 开始决策
                                            time_start = time.time()
                                            t = random.randint(8, 18)
                                            t = t + 0.1 * random.randint(0, 9)
                                            time.sleep(t)
                                            interface_cmd("S1")
                                            # 格式(已确定)和本地的调用（待完成）
                                            # print("变换S1的网络接口")  # 决策内容
                                            
                                            i_decision = "变换S1的网络接口"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_ip_decision(i_decision) values(%s)", [i_decision])
                                            cur.close()
                                            conn.commit()
                                            time_end = time.time()
                                            t += time_end - time_start  # 累加前期的时间
                                        elif targethost == 'S2' or targethost == 's2':
                                            # print("攻击者对S2发动了DoS攻击")  # 展示到威胁感知界面上的攻击步骤中
                                            step = "攻击者对S2发动了DoS攻击"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                            cur.close()
                                            conn.commit()
                                            
                                            # 开始决策
                                            time_start = time.time()
                                            time_start = time.time()
                                            t = random.randint(8, 18)
                                            t = t + 0.1 * random.randint(0, 9)
                                            time.sleep(t)
                                            interface_cmd("S2")
                                            # 格式(已确定)和本地的调用（待完成）
                                            # print("变换S2的网络接口")  # 决策内容
                                            
                                            i_decision = "变换S2的网络接口"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_ip_decision(i_decision) values(%s)", [i_decision])
                                            cur.close()
                                            conn.commit()
                                            time_end = time.time()
                                            t += time_end - time_start  # 累加前期的时间
                                        else:
                                            # print("攻击者对" + targethost + "发动了DoS攻击")  # 展示到威胁感知界面上的攻击步骤中
                                            step = "攻击者对" + targethost + "发动了DoS攻击"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                            cur.close()
                                            conn.commit()
                                            # 对其他IP发动攻击是无效的，不决策
                                    decision_time = str('%.3f' % t) + "s"
                                    print(decision_time, 's')
                                    cur = conn.cursor()
                                    cur.execute("insert into presentation_decision_time(decision_time) values(%s)", [decision_time])
                                    cur.close()
                                    conn.commit()
                                else:  # 其他情况，说明处于攻击的中间阶段
                                    if attacktype == 'one_ip_scan':
                                        if targethost == 'S1' or targethost == 's1':  # 兼容大小写，防止错误
                                            if detail == 'reachable':
                                                # print("攻击者对S1的真实IP进行了扫描")  # 展示到威胁感知界面上的攻击步骤中
                                                step = "攻击者对S1的真实IP进行了扫描"
                                                cur = conn.cursor()
                                                cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                                cur.close()
                                                conn.commit()
                                                
                                                # 开始决策
                                                time_start = time.time()
                                                time_start = time.time()
                                                t = random.randint(8, 18)
                                                t = t + 0.1 * random.randint(0, 9)
                                                time.sleep(t)
                                                network_cmd("S1", 0)
                                                port_cmd("S1", 0)
                                                # print("变换S1的IP地址与端口")  # 决策内容
                                                
                                                i_decision = "变换S1的IP地址与端口"
                                                cur = conn.cursor()
                                                cur.execute("insert into presentation_ip_decision(i_decision) values(%s)", [i_decision])
                                                cur.close()
                                                conn.commit()
                                                time_end = time.time()
                                                t += time_end - time_start  # 累加前期的时间
                                            elif detail == 'unreachable':
                                                # print("攻击者对S1的虚假IP进行了扫描")  # 展示到威胁感知界面上的攻击步骤中
                                                step = "攻击者对S1的虚假IP进行了扫描"
                                                cur = conn.cursor()
                                                cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                                cur.close()
                                                conn.commit()
                                                # 无决策
                                        elif targethost == 'S2' or targethost == 's2':  # 兼容大小写，防止错误
                                            if detail == 'reachable':
                                                # print("攻击者对S2的真实IP进行了扫描")  # 展示到威胁感知界面上的攻击步骤中
                                                step = "攻击者对S2的真实IP进行了扫描"
                                                cur = conn.cursor()
                                                cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                                cur.close()
                                                conn.commit()
                                                
                                                # 开始决策
                                                time_start = time.time()
                                                time_start = time.time()
                                                t = random.randint(8, 18)
                                                t = t + 0.1 * random.randint(0, 9)
                                                time.sleep(t)
                                                network_cmd("S2", 0)
                                                port_cmd("S2", 0)
                                                # print("变换S2的IP地址与端口")  # 决策内容
                                                
                                                i_decision = "变换S2的IP地址与端口"
                                                cur = conn.cursor()
                                                cur.execute("insert into presentation_ip_decision(i_decision) values(%s)", [i_decision])
                                                cur.close()
                                                conn.commit()
                                                time_end = time.time()
                                                t += time_end - time_start  # 累加前期的时间
                                            elif detail == 'unreachable':
                                                # print("攻击者对S2的虚假IP进行了扫描")  # 展示到威胁感知界面上的攻击步骤中
                                                step = "攻击者对S2的虚假IP进行了扫描"
                                                cur = conn.cursor()
                                                cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                                cur.close()
                                                conn.commit()
                                                # 无决策
                                        else:  # 扫描其他地址时
                                            # print("攻击者对" + targethost + "进行了扫描")
                                            step = "攻击者对" + targethost + "进行了扫描"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                            cur.close()
                                            conn.commit()
                                            # 开始决策
                                            time_start = time.time()
                                            time_start = time.time()
                                            t = random.randint(8, 18)
                                            t = t + 0.1 * random.randint(0, 9)
                                            time.sleep(t)
                                            network_cmd("S1", 0)
                                            network_cmd("S2", 0)
                                            port_cmd("S1", 0)
                                            port_cmd("S2", 0)
                                            # print("变换S1与S2的IP地址与端口")  # 决策内容
                                            
                                            i_decision = "变换S1与S2的IP地址与端口"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_ip_decision(i_decision) values(%s)", [i_decision])
                                            cur.close()
                                            conn.commit()
                                            time_end = time.time()
                                            t += time_end - time_start  # 累加前期的时间
                                    elif attacktype == 'file_modify':
                                        if "data" in targetlayer:
                                            if targethost == 'S1' or targethost == 's1':
                                                # print("攻击者对S1上的样例数据进行了访问和修改")  # 展示到威胁感知界面上的攻击步骤中
                                                step = "攻击者对S1上的样例数据进行了访问和修改"
                                                cur = conn.cursor()
                                                cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                                cur.close()
                                                conn.commit()
                                                
                                                # 开始决策
                                                time_start = time.time()
                                                # 恢复命令待完成
                                                # print("从S1备份中恢复数据，并随机生成新密钥对备份重新加密")  # 决策内容
                                                # print("更改S1上数据的访问权限")  # 这部分是否需要？
                                                data_cmd("random")
                                                data_cmd("permission")
                                                
                                                d_decision = "对S1中数据进行备份，并生成随机密钥对备份加密"
                                                cur = conn.cursor()
                                                cur.execute("insert into presentation_data_decision(d_decision) values(%s)", [d_decision])
                                                cur.close()
                                                conn.commit()
                                                time_end = time.time()
                                                t = t + time_end - time_start  # 累加前期的时间
                                            elif targethost == 'S2' or targethost == 's2':
                                                # print("攻击者对S2上的样例数据进行了访问和修改")  # 展示到威胁感知界面上的攻击步骤中
                                                step = "攻击者对S2上的样例数据进行了访问和修改"
                                                cur = conn.cursor()
                                                cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                                cur.close()
                                                conn.commit()
                                                # 开始决策
                                                time_start = time.time()
                                                time_start = time.time()
                                                t = random.randint(8, 18)
                                                t = t + 0.1 * random.randint(0, 9)
                                                time.sleep(t)
                                                # 恢复命令待完成
                                                # print("从S2备份中恢复数据，并随机生成新密钥对备份重新加密")  # 决策内容
                                                # print("更改S2上数据的访问权限")  # 这部分是否需要？
                                                data_cmd("random")
                                                data_cmd("permission")
                                                
                                                d_decision = "对S2中数据进行备份，并生成随机密钥对备份加密"
                                                cur = conn.cursor()
                                                cur.execute("insert into presentation_data_decision(d_decision) values(%s)", [d_decision])
                                                cur.close()
                                                conn.commit()                                                
                                                time_end = time.time()
                                                t += time_end - time_start  # 累加前期的时间
                                        elif "software" in targetlayer:
                                            if targethost == 'S1' or targethost == 's1':
                                                if detail == 'java':
                                                    step = "攻击者对S1上的java源文件进行了访问和修改"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                                    cur.close()
                                                    conn.commit()
                                                    # 开始决策
                                                    time_start = time.time()
                                                    software_cmd("version", "java")
                                                    runtime_cmd(2)

                                                    s_decision = "针对S1上的java版本进行变换"
                                                    r_decision = "对S1启动完全的ASLR与ISR随机化"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_runtime_decision(r_decision) values(%s)", [r_decision])
                                                    cur.execute("insert into presentation_software_decision(s_decision) values(%s)", [s_decision])
                                                    cur.close()
                                                    conn.commit()
                                                    time_end = time.time()
                                                    t += time_end - time_start  # 累加前期的时间
                                                elif detail == 'php':
                                                    step = "攻击者对S1上的php源文件进行了访问和修改"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                                    cur.close()
                                                    conn.commit()
                                                    # 开始决策
                                                    time_start = time.time()
                                                    software_cmd("version", "php")
                                                    runtime_cmd(1)
                                                    # print("针对S1上的php版本进行变换，并对S1启动保留的ASLR与ISR随机化")  # 决策内容
                                                    
                                                    s_decision = "针对S1上的php版本进行变换"
                                                    r_decision = "对S1启动保留的ASLR与ISR随机化"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_runtime_decision(r_decision) values(%s)", [r_decision])
                                                    cur.execute("insert into presentation_software_decision(s_decision) values(%s)", [s_decision])
                                                    cur.close()
                                                    conn.commit() 
                                                    time_end = time.time()
                                                    t += time_end - time_start  # 累加前期的时间
                                                elif detail == 'pdf':
                                                    step = "攻击者对S1上的pdf文件进行了访问和修改"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                                    cur.close()
                                                    conn.commit()
                                                    # 开始决策
                                                    time_start = time.time()
                                                    software_cmd("type", "pdf")
                                                    # print("针对S1上的pdf软件类型进行变换")  # 决策内容
                                                    
                                                    s_decision = "针对S1上的pdf软件类型进行变换"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_software_decision(s_decision) values(%s)", [s_decision])
                                                    cur.close()
                                                    conn.commit()
                                                    time_end = time.time()
                                                    t += time_end - time_start  # 累加前期的时间
                                                elif detail == 'browser':
                                                    step = "攻击者对S1上的html源文件进行了访问和修改"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                                    cur.close()
                                                    conn.commit()
                                                    # 开始决策
                                                    time_start = time.time()
                                                    software_cmd("type", "browser")
                                                    # print("针对S1上的browser软件类型进行变换")  # 决策内容
                                                    
                                                    s_decision = "针对S1上的browser软件类型进行变换"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_software_decision(s_decision) values(%s)", [s_decision])
                                                    cur.close()
                                                    conn.commit()
                                                    time_end = time.time()
                                                    t += time_end - time_start  # 累加前期的时间
                                            elif targethost == 'S2' or targethost == 's2':
                                                if detail == 'java':
                                                    step = "攻击者对S2上的java源文件进行了访问和修改"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                                    cur.close()
                                                    conn.commit()
                                                    # 开始决策
                                                    time_start = time.time()
                                                    time_start = time.time()
                                                    t = random.randint(8, 18)
                                                    t = t + 0.1 * random.randint(0, 9)
                                                    time.sleep(t)
                                                    software_cmd("version", "java")
                                                    runtime_cmd(2)
                                                    #print("针对S2上的java版本进行变换，并对S2启动完全的ASLR与ISR随机化")  # 决策内容
                                                    
                                                    s_decision = "针对S2上的java版本进行变换"
                                                    r_decision = "对S2启动完全的ASLR与ISR随机化"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_runtime_decision(r_decision) values(%s)", [r_decision])
                                                    cur.execute("insert into presentation_software_decision(s_decision) values(%s)", [s_decision])
                                                    cur.close()
                                                    conn.commit()   
                                                    time_end = time.time()
                                                    t += time_end - time_start  # 累加前期的时间
                                                elif detail == 'php':
                                                    step = "攻击者对S2上的php源文件进行了访问和修改"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                                    cur.close()
                                                    conn.commit()
                                                    # 开始决策
                                                    time_start = time.time()
                                                    time_start = time.time()
                                                    t = random.randint(8, 18)
                                                    t = t + 0.1 * random.randint(0, 9)
                                                    time.sleep(t)
                                                    software_cmd("version", "php")
                                                    runtime_cmd(1)
                                                    # print("针对S2上的php版本进行变换，并对S2启动保留的ASLR与ISR随机化")  # 决策内容
                                                    
                                                    s_decision = "针对S2上的php版本进行变换"
                                                    r_decision = "对S2启动保留的ASLR与ISR随机化"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_runtime_decision(r_decision) values(%s)", [r_decision])
                                                    cur.execute("insert into presentation_software_decision(s_decision) values(%s)", [s_decision])
                                                    cur.close()
                                                    conn.commit()
                                                    time_end = time.time()
                                                    t += time_end - time_start  # 累加前期的时间
                                                elif detail == 'pdf':
                                                    step = "攻击者对S2上的pdf文件进行了访问和修改"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                                    cur.close()
                                                    conn.commit()
                                                    # 开始决策
                                                    time_start = time.time()
                                                    software_cmd("type", "pdf")
                                                    # print("针对S2上的pdf软件类型进行变换")  # 决策内容
                                                    
                                                    s_decision = "针对S2上的pdf软件类型进行变换"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_software_decision(s_decision) values(%s)", [s_decision])
                                                    cur.close()
                                                    conn.commit()
                                                    time_end = time.time()
                                                    t += time_end - time_start  # 累加前期的时间
                                                elif detail == 'browser':
                                                    step = "攻击者对S2上的html源文件进行了访问和修改"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                                    cur.close()
                                                    conn.commit()
                                                    # 开始决策
                                                    time_start = time.time()
                                                    time_start = time.time()
                                                    t = random.randint(8, 18)
                                                    t = t + 0.1 * random.randint(0, 9)
                                                    time.sleep(t)
                                                    software_cmd("type", "browser")
                                                    # print("针对S2上的browser软件类型进行变换")  # 决策内容
                                                    
                                                    s_decision = "针对S2上的browser软件类型进行变换"
                                                    cur = conn.cursor()
                                                    cur.execute("insert into presentation_software_decision(s_decision) values(%s)", [s_decision])
                                                    cur.close()
                                                    conn.commit()
                                                    time_end = time.time()
                                                    t += time_end - time_start  # 累加前期的时间
                                    elif attacktype == 'vm_remote_connection':
                                        if targethost == 'S1' or targethost == 's1':
                                            step = "攻击者对S1上提供服务的虚拟机进行了访问"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                            cur.close()
                                            conn.commit()
                                            # 开始决策
                                            time_start = time.time()
                                            time_start = time.time()
                                            t = random.randint(8, 18)
                                            t = t + 0.1 * random.randint(0, 9)
                                            time.sleep(t)
                                            os_cmd("S1", 2)
                                            # print("针对S1上运行的虚拟机进行系统版本和位数的变换")  # 决策内容
                                            
                                            o_decision = "针对S1上运行的虚拟机进行系统版本和位数的变换"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_os_decision(o_decision) values(%s)", [o_decision])
                                            cur.close()
                                            conn.commit()
                                            time_end = time.time()
                                            t += time_end - time_start  # 累加前期的时间
                                        elif targethost == 'S2' or targethost == 's2':
                                            step = "攻击者对S2上提供服务的虚拟机进行了访问"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_attack_step(step) values(%s)", [step])
                                            cur.close()
                                            conn.commit()
                                            # 开始决策
                                            time_start = time.time()
                                            time_start = time.time()
                                            t = random.randint(8, 18)
                                            t = t + 0.1 * random.randint(0, 9)
                                            time.sleep(t)
                                            os_cmd("S2", 3)
                                            # print("针对S2上运行的虚拟机进行系统类型、版本、位数的变换")  # 决策内容
                                            
                                            o_decision = "针对S2上运行的虚拟机进行系统版本和位数的变换"
                                            cur = conn.cursor()
                                            cur.execute("insert into presentation_os_decision(o_decision) values(%s)", [o_decision])
                                            cur.close()
                                            conn.commit()
                                            time_end = time.time()
                                            t += time_end - time_start  # 累加前期的时间

                                # cur.close()
                                # conn.commit()
                                # conn.close()
                                # 执行完后删除Attackreport内的json文件
                # 将此次的决策命令发送，并在发送后将本地删除
                p = os.getcwd() + '/JsonFileCmd/'  # 获取当前工程下的JsonFileCmd目录
                if not os.listdir(p):  # JsonFileCmd目录为空
                    pass
                else:
                    tcpclient_sendcmd.send()
                    time.sleep(5)
                    d = os.listdir(p)
                    for n in d:  # 循环读取路径下的文件
                        os.remove(p+n)  # 删除JSON文件
                
                time.sleep(10)       
                deleteFile = os.path.join(path, i)
                os.remove(deleteFile)  # 删除JSON文件

                     
             
    
    conn.close()