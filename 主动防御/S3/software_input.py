#!/usr/bin/env python
# coding=utf-8
# 将软件层的变换前后数据写入数据库

import MySQLdb
import time
import os
import json


def software():
	# 连接数据库
	conn = MySQLdb.connect(
	                host='localhost',
	                port=3306,
	                user='root',
	                passwd='cnv6201',
	                db='PRESENTATION',
	                charset='utf8',
	            )
	cur = conn.cursor()
	# 清空数据库中表格内的信息
	cur.execute("delete from presentation_software")
	# 打开software层JSON文件读取数据
	f = open('JsonFileReport/report_software_before.json', "r", encoding='UTF-8')
	d = open('JsonFileReport/report_software_after.json', "r", encoding='UTF-8')
	before = json.load(f)
	after = json.load(d)

	# 向数据库中传入software层变换前版本信息及应用信息
	pdf_before = before['layer_info']['type']['pdf']
	browser_before = before['layer_info']['type']['browser']
	java_before = before['layer_info']['version']['java']
	php_before = before['layer_info']['version']['php']

	# 向数据库中传入software层变换后版本信息及应用信息
	pdf_after = after['layer_info']['type']['pdf']
	browser_after = after['layer_info']['type']['browser']
	java_after = after['layer_info']['version']['java']
	php_after = after['layer_info']['version']['php']
	cur.execute("insert into presentation_software(pdf_before, browser_before, java_before, php_before, "
	            "pdf_after, browser_after, java_after, php_after) values(%s, %s, %s, %s, %s, %s, %s, %s)",
	            [pdf_before, browser_before, java_before, php_before, pdf_after, browser_after, java_after, php_after])
	d.close()
	f.close()
	cur.close()
	conn.commit()
	conn.close()
	print("software层数据存储成功")
