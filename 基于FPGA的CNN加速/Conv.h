#ifndef _CONV_H_
#define _CONV_H_
#include "constant.h"
#include "ap_int.h"
float relu(float );

void CONVOLUTION_LAYER_1(const float input_feature[image_Batch*INPUT_CHANNEL*CONV_1_INPUT_SIZE],
        const float weights[INPUT_CHANNEL*FILTER_SIZE+INPUT_CHANNEL*CONV_1_TYPE],
        const float bias[CONV_1_TYPE],
        float output_feature[image_Batch*CONV_1_TYPE*CONV_1_OUTPUT_SIZE],
        int init
        );


void CONVOLUTION_LAYER_2(const float input_feature[image_Batch*CONV_1_TYPE*CONV_1_OUTPUT_SIZE],
        const float weights[CONV_1_TYPE*FILTER_SIZE+CONV_2_TYPE*CONV_1_TYPE],
        const float bias[CONV_2_TYPE],
        float output_feature[image_Batch*CONV_2_TYPE*CONV_2_OUTPUT_SIZE],
        int init
        );

#endif
