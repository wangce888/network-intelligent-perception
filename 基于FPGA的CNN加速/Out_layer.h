#include "constant.h"

float expf(float );
int softmax(float input_feature[Category]);

void GAP(float input_feature[image_Batch*CONV_2_TYPE*CONV_2_OUTPUT_SIZE],
        float output_feature[image_Batch*CONV_2_TYPE]
        );

void OUTPUT_LAYER(float input_feature[image_Batch*CONV_2_TYPE],
            float weights[CONV_2_TYPE*Category],
            float bias[Category],
            int output_feature[image_Batch]);
