#ifndef HASHFUNCTION_H_INCLUDED
#define HASHFUNCTION_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include "ABFStruct.h"
#define BITS_IN_int     ( sizeof(int) * CHAR_BIT )
#define THREE_QUARTERS  ((int) ((BITS_IN_int * 3) / 4))
#define ONE_EIGHTH      ((int) (BITS_IN_int / 8))
#define HIGH_BITS       ( ~((unsigned int)(~0) >> ONE_EIGHTH ))

/***************
command=0;查询
command=1;插入
****************/
int HashFunctionNum = 0;//控制使用的hash函数个数
int Flag = 0;
int Count = 0;//查询时记录元素个数
//调用一次Hash函数都需要初始化HashFunctionNum，以及将Flag置为1
void GetHashFunctionNum(int num)
{
   HashFunctionNum = num;
   Flag = 1;
   Count = 0;
}

int GetCount()
{
    return Count;
}

int Operation(AdBloomFilter ABF, int HashNum, int Index ,int command)
{
    int temp = ABF.DictHash[Index];
    printf("Index:%d  HashNum:%d  Temp:%d\n",Index,HashNum,temp);
    if(command == 1){ //插入
        if(temp == 0){
            Flag = 0;
            ABF.DictHash[Index] = 1;
        }
        if(HashNum == HashFunctionNum && Flag == 1)
            //哈希函数个数等于初始k个，k位值均为1，计算k+1个散列值
            HashFunctionNum++;

	}else{//查询command=0
	    if(temp == 0)
            Flag = 0;
	    if(Flag == 1 && HashNum == HashFunctionNum){
            Count++;
            HashFunctionNum++;
	    }else
            return 0;
	}
	return 0;
}
/***********************************************
	函数名称: bool hash1(char* buffer, unsigned int*& dicthash, int command=0)
	函数功能: 将字符串按照ascii码变为数字，并得到其积，最后对比特长度取余，
               如果int参数为1则将bloomfilter对应标志位置1，否则跳过。之后查询该位置是否为1.
	传入参数: char* buffer--目标字符串，int command=0--查询，=1--更改
	返回值: 如果是1返回true，否则为false。
************************************************/
int hash1(char* buffer, AdBloomFilter ABF, int command)
{
    if(HashFunctionNum < 1)
        return 1;

	unsigned int tmp = 1;
	while (*buffer != '\0')
	{
		tmp *= (int)(*buffer);
		tmp %= ABF.ABF_Bit_Count;
		buffer++;
	}
	return Operation(ABF, 1, tmp % ABF.ABF_Bit_Count, command);
}

/***********************************************
	函数名称: bool hash2(char* buffer, unsigned  int*& dicthash, int command = 0)
	函数功能: 将字符串按照ascii码变为数字，并得到其和，最后对比特长度取余，
               如果int参数为1则将bloomfilter对应标志位置1，否则跳过。之后查询该位置是否为1.
	传入参数: char* buffer--目标字符串，int command=0--查询，=1--更改
	返回值: 如果是1返回true，否则为false。
************************************************/
int hash2(char* buffer,AdBloomFilter ABF, int command )
{
    if(HashFunctionNum < 2)
        return 1;

	unsigned int tmp = 0;
	while (*buffer != '\0')
	{
		tmp += (int)(*buffer);

		buffer++;
	}
	return Operation(ABF, 2, tmp % ABF.ABF_Bit_Count, command);
}
/***********************************************
	函数名称: bool hash3(char* buffer, unsigned  int*& dicthash, int command = 0)
	函数功能: 将字符串按照ascii码变为数字，并得到平方根取整的和，最后对比特长度取余，
               如果int参数为1则将bloomfilter对应标志位置1，否则跳过。之后查询该位置是否为1.
	传入参数: char* buffer--目标字符串，int command=0--查询，=1--更改
	返回值: 如果是1返回true，否则为false。
************************************************/
int hash3(char* buffer,AdBloomFilter ABF, int command )
{
	if(HashFunctionNum < 3)
        return 1;

	unsigned int tmp = 1;
	while (*buffer != '\0')
	{
		tmp *= (int)(sqrt((double)*buffer));

		buffer++;
	}
	return Operation(ABF, 3, tmp % ABF.ABF_Bit_Count, command);
}
/***********************************************
	函数名称: bool hash34char* buffer, unsigned  int*& dicthash, int command = 0)
	函数功能: 将字符串按照ascii码变为数字，按+-+-。。。顺序依次加减，最后对比特长度取余，
               如果int参数为1则将bloomfilter对应标志位置1，否则跳过。之后查询该位置是否为1.
	传入参数: char* buffer--目标字符串，int command=0--查询，=1--更改
	返回值: 如果是1返回true，否则为false。
************************************************/
int hash4(char* buffer,AdBloomFilter ABF, int command )
{
    if(HashFunctionNum < 4)
        return 1;

	unsigned int tmp = 0;
	unsigned int sign = 1;
	while (*buffer != '\0')
	{
		tmp += sign*(int)(*buffer);
		sign *= -1;
		buffer++;
	}
	return Operation(ABF, 4, tmp % ABF.ABF_Bit_Count ,command);
}

//BKDR Hash Function
int hash5(char* buffer,AdBloomFilter ABF, int command )
{
    if(HashFunctionNum < 5)
        return 1;

    unsigned int tmp = 0;
    unsigned int sign = 131;

    while(*buffer){
        tmp = tmp *sign + (*buffer++);
    }
    tmp = tmp & 0x7FFFFFFF;

    return Operation(ABF, 5, tmp % ABF.ABF_Bit_Count ,command);
}

//AP Hash Function
int hash6(char* buffer,AdBloomFilter ABF, int command)
{
    if(HashFunctionNum < 6)
        return 1;

    unsigned int tmp = 0;
    int i;

    for(i = 0; *buffer; i++){
        if ((i & 1) == 0){
			tmp ^= ((tmp << 7) ^ (*buffer++) ^ (tmp >> 3));
		}
		else{
			tmp ^= (~((tmp << 11) ^ (*buffer++) ^ (tmp >> 5)));
		}
    }
    tmp = tmp & 0x7FFFFFFF;

    return Operation(ABF, 6, tmp % ABF.ABF_Bit_Count ,command);
}

//DJB Hash function
int hash7(char* buffer,AdBloomFilter ABF, int command)
{
    if(HashFunctionNum < 7)
        return 1;

    unsigned int tmp = 5381;

    while (*buffer){
		tmp += (tmp << 5) + (*buffer++);
	}

	tmp = tmp & 0x7FFFFFFF;

	return Operation(ABF, 7, tmp % ABF.ABF_Bit_Count ,command);
}

//JS Hash Function
int hash8(char* buffer,AdBloomFilter ABF, int command)
{
    if(HashFunctionNum < 8)
        return 1;

    unsigned int tmp = 1315423911;

	while (*buffer)
	{
		tmp ^= ((tmp << 5) + (*buffer++) + (tmp >> 2));
	}

	tmp = tmp & 0x7FFFFFFF;

	return Operation(ABF, 8, tmp % ABF.ABF_Bit_Count ,command);
}

//RS Hash Function
int hash9(char* buffer,AdBloomFilter ABF, int command)
{
    if(HashFunctionNum < 9)
        return 1;

    unsigned int tmp = 0;
	unsigned int b = 378551;
	unsigned int a = 63689;

	while (*buffer)
	{
		tmp = tmp * a + (*buffer++);
		a *= b;
	}

	tmp = tmp & 0x7FFFFFFF;

	return Operation(ABF, 9, tmp % ABF.ABF_Bit_Count ,command);
}

//SDBM Hash Function
int hash10(char* buffer,AdBloomFilter ABF, int command)
{
    if(HashFunctionNum < 10)
        return 1;

    unsigned int tmp = 0;

	while (*buffer)
	{
		// equivalent to: hash = 65599*hash + (*str++);
		tmp = (*buffer++) + (tmp << 6) + (tmp << 16) - tmp;
	}

	tmp = tmp & 0x7FFFFFFF;

	return Operation(ABF, 10, tmp % ABF.ABF_Bit_Count ,command);
}

//FNV Hash Function
int hash11(char* buffer,AdBloomFilter ABF, int command)
{
    if(HashFunctionNum < 11)
        return 1;

    unsigned int tmp = 0;

	int fnvprime = 0x811C9DC5;

	while (*buffer) {
		tmp *= fnvprime;
		tmp ^= (int)(*buffer++);
	}

	tmp = tmp & 0x7FFFFFFF;

	return Operation(ABF, 11, tmp % ABF.ABF_Bit_Count ,command);
}

//JAVA string hash function
int hash12(char* buffer, AdBloomFilter ABF, int command)
{
    if(HashFunctionNum < 12)
        return 1;

    unsigned int tmp = 0;

	while (*buffer) {
		tmp = tmp * 31 + (*buffer++);
	}

	tmp = tmp & 0x7FFFFFFF;

	return Operation(ABF, 12 , tmp % ABF.ABF_Bit_Count ,command);
}

//ELF HASH
int hash13(char* buffer, AdBloomFilter ABF, int command)
{
    if(HashFunctionNum < 13)
        return 1;

	unsigned int tmp = 1;
	unsigned int g;
	while (*buffer != '\0')
	{
		tmp = (tmp<<4) + *buffer++;
        if ((g = (tmp & 0xf0000000)))
            tmp ^=g>>24;
        tmp &=~g;
	}
	return Operation(ABF, 13, tmp % ABF.ABF_Bit_Count, command);
}
//PJW HASH
int hash14(char* buffer,AdBloomFilter ABF, int command )
{
    if(HashFunctionNum < 14)
        return 1;

	unsigned int tmp = 0,i;
	for(tmp = 0; *buffer; ++buffer)
    {
        tmp = (tmp << ONE_EIGHTH)+*buffer;
        if((i = tmp & HIGH_BITS)!=0)
            tmp = (tmp ^ (i >> THREE_QUARTERS)) & ~HIGH_BITS;
    }
	return Operation(ABF, 14, tmp % ABF.ABF_Bit_Count, command);
}

int Insert(char * buffer)
{
    GetHashFunctionNum(ABF.ABF_Hash_Num);
    hash1(buffer, ABF, 1);
    hash2(buffer, ABF, 1);
    hash3(buffer, ABF, 1);
    hash4(buffer, ABF, 1);
    hash5(buffer, ABF, 1);
    hash6(buffer, ABF, 1);
    hash7(buffer, ABF, 1);
    hash8(buffer, ABF, 1);
    hash9(buffer, ABF, 1);
    hash10(buffer, ABF, 1);
    hash11(buffer, ABF, 1);
    hash12(buffer, ABF, 1);
    hash13(buffer, ABF, 1);
    hash14(buffer, ABF, 1);
    return 0;
}

int Query(char * buffer)
{
    int Count;
    GetHashFunctionNum(ABF.ABF_Hash_Num);
    hash1(buffer, ABF, 0);
    hash2(buffer, ABF, 0);
    hash3(buffer, ABF, 0);
    hash4(buffer, ABF, 0);
    hash5(buffer, ABF, 0);
    hash6(buffer, ABF, 0);
    hash7(buffer, ABF, 0);
    hash8(buffer, ABF, 0);
    hash9(buffer, ABF, 0);
    hash10(buffer, ABF, 0);
    hash11(buffer, ABF, 0);
    hash12(buffer, ABF, 0);
    hash13(buffer, ABF, 0);
    hash14(buffer, ABF, 0);
    Count = GetCount();
    if(Count == 0)//元素不存在
        printf("Element does not exist!\n");
    else
        printf("Element exists! Count:%d\n", Count);//元素存在，输出出现次数
    return 0;
}

int ReadFile_Insert()
{
    FILE* fp = NULL;
    fp = fopen("E:\\bloomFilter\\text.csv", "r");
    if (fp != NULL)
        printf("YES!!\n");
    else{
        printf("NO!!\n");
        return 0;
    }
    char row[1024];
    char *token;
    while (fgets(row, 1024, fp) != NULL){
        //printf("Row: %s", row);
        token = strtok(row, ",");
        printf("\nToken:%s", token);
        Insert(token);
    }
    printf("\nElement insertion completed!\n");
    return 0;
}

int ReadFile_Query()
{
    FILE* fp = NULL;
    fp = fopen("E:\\bloomFilter\\text2.csv", "r");
    if (fp != NULL)
        printf("YES!!\n");
    else{
        printf("NO!!\n");
        return 0;
    }
    char row[1024];
    char *token;
    while (fgets(row, 1024, fp) != NULL){
        //printf("Row: %s", row);
        token = strtok(row, ",");
        printf("\nToken:%s", token);
        Query(token);
    }
    printf("\nElement query completed!\n");
    return 0;
}
#endif // HASHFUNCTION_H_INCLUDED
