#ifndef CBFSTRUCT_H_INCLUDED
#define CBFSTRUCT_H_INCLUDED

#include "HashFunction.h"
#include "Function.h"
typedef struct DataCount{
    unsigned short Bit;
    int Count;
}DataCount;

typedef struct CountBloomFilter{
    DataCount *DictHash;
    int CBF_Bit_Count;
    int CBF_Hash_Num;
}CountBloomFilter;

CountBloomFilter CBF;

int InitCBF()
{
    printf("Please enter Bloom Filter size:");
    scanf("%d",&CBF.CBF_Bit_Count);
    printf("Please enter the number of hash functions:");
    scanf("%d",&CBF.CBF_Hash_Num);
    GetHashFunctionNum(CBF.CBF_Hash_Num);
    CBF.DictHash = (DataCount *)malloc(CBF.CBF_Bit_Count*sizeof(DataCount));
    if(CBF.DictHash)
        printf("Initializing successful!\n");
    else
        printf("Initialization failed!\n");
    memset(CBF.DictHash, 0, CBF.CBF_Bit_Count);
    return 0;
}

int Destory()
{
    free(CBF.DictHash);
    return 0;
}


 void Print()
 {
     int i;
     for(i = 0; i < CBF.CBF_Bit_Count; i++)
     {
         printf("%-5d:%-5u,%-5d\n",i, CBF.DictHash[i].Bit, CBF.DictHash[i].Count);
     }
 }
#endif // CBFSTRUCT_H_INCLUDED
