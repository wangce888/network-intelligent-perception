# Bloom Filter

#### 介绍

该开源项目为东南大学网络空间安全学院必修课“网络测量学”课程内必修实验项目。

Bloom Filter是由Howard Bloom在1970年提出的，实际上是一个很长的二进制向量和一系列随机映射函数。

配套教材：《网络测量学》东南大学出版社


#### 实验基本原理

Bloom Filter是由Howard Bloom在1970年提出的，实际上是一个很长的二进制向量和一系列随机映射函数。本质上布隆过滤器是一种数据结构，比较巧妙的概率型数据结构（probabilistic data structure）高效地插入和查询，可以用来判断某个元素在集合中一定不存在或者可能存在。比于传统的 List、Set、Map 等数据结构，它更高效、占用空间更少。Bloom Filter 采用的是哈希函数的方法来判断一个元素是否在集合中，将一个元素映射到一个 m 长度的阵列上的k个点，当这k个点的值均为1时，那么这个元素就在集合内。Bloom Filter 优点是插入和查询元素的时间都是常数，另外它插入元素却并不保存元素本身，具有良好的安全性。但它的缺点也显而易见，随着插入元素增多，错判某元素“在集合内”的概率越大，并且返回的结果是概率性的，而不是确切的。
标准的Bloom Filter是一种很简单的数据结构，它只支持插入和查找两种操作。在所要表达的集合是静态集合的时候，标准Bloom Filter可以很好地工作，但是如果要表达的集合经常变动，标准Bloom Filter的弊端就显现出来了，因为它不支持删除操作。Counting Bloom Filter（CBF）的出现解决了这个问题，它将标准Bloom Filter位数组的每一位扩展为一个小的计数器（Counter），在插入元素时给对应的k（k为哈希函数个数）个Counter的值分别加1，删除元素时给对应的k个Counter的值分别减1。Counting Bloom Filter通过多占用几倍的存储空间的代价，给Bloom Filter增加了删除操作。
Adaptive Bloom Filter（ABF）与Bloom Filter（BF）一样，仅使用单个位向量。ABF可以以与CBF相同的方式估计每个元素出现的次数。ABF动态更改哈希函数的数量。在ABF上计算哈希冲突的基本思想如下：ABF工作在BF的基础上 ，即ABF将k个哈希函数指示的每个位位置设置为1。当所有k个位位置都已设置为1以记录一个元素时，ABF通过使用第k+1个附加哈希函数来计算哈希值。此外，如果由第k+1个函数定位的位已经是1，则ABF通过另一个哈希函数迭代计算，直到由第k+N+1个哈希函数指示的位为0，附加散列函数的数量N代表每个关键元素的出现次数。

#### 软件架构

HashFunction.h中给出了Bloom Filter、Count Bloom Filter和Adaptive Bloom Filter所使用的的哈希函数。
BFStruct.h定义了Bloom Filter数据结构及初始化操作。
CBFStruct.h定义了Count Bloom Filter数据结构及初始化操作。
ABFStruct.h定义了Adaptive Bloom Filter数据结构及初始化操作。


#### 使用说明
相应实验的复现，可参考相关论文或者作者上传的实验报告
