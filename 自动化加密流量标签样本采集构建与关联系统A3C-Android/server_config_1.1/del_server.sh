#!/usr/bin/env bash
# 本文件用于删除指定虚拟接口，并关闭隧道
# 需要root权限，使用bash命令执行此文件

# 检测执行用户权限是否为root
if [ $UID != 0 ]
then
    echo "错误！未使用root权限执行！"
    exit 0
fi

file_path="./virtual_interface.csv"  # 虚拟接口配置文件
log_path="./log/"  # 日志目录

# 检测文件是否存在且不为空，不符合则退出
if [ -s ${file_path} ]
then
    # 按行打印文件（含行号）
    echo "虚拟接口配置信息列表如下，请按Ctrl+D结束显示："
    echo "编号 TUN接口名称 进程号PID 客户端地址 服务器地址"
    cat -n ${file_path}
    while read line
    do
        echo $line
    done
else
    echo "文件不存在或为空"
    exit 1
fi

###### 此处需要用户手动键入Ctrl+D ######

# 获取用户指定删除行号
echo -n "请输入您想要删除的虚拟接口编号："
read num
int=$(awk 'END {print NR}' ${file_path})  # 读取文件已有行数
if ((${num} <= 0))
then
    echo "错误！选择接口数量需大于0！"
    exit 2
elif ((${num} > ${int}))
then
    echo "错误！选择接口数量超出上限！"
    exit 3
else
    # 获取用户指定删除虚拟接口名称和进程号
    tun_name=$(awk -F, 'NR=='${num}' {print $1}' ${file_path})
    pid_num=$(awk -F, 'NR=='${num}' {print $2}' ${file_path})
    # 结束VPN进程
    kill ${pid_num}
    # 删除虚拟接口
    ip tuntap del dev ${tun_name} mode tun
    # 删除指定行
    sed -i ''${num}'d' ${file_path}
    echo "您已成功删除虚拟接口"${num}":"${tun_name}
    # 日志文件
    log_time=$(date "+%Y-%m-%d %H:%M:%S")
    log_name=${log_path}${tun_name}".txt"
    echo "TUN接口关闭时间："${log_time} >> ${log_name}
fi