#!/usr/bin/env bash
# 本文件用于批量删除虚拟接口，并关闭隧道
# 需要root权限，使用bash命令执行此文件

# 检测执行用户权限是否为root
if [ $UID != 0 ]
then
    echo "错误！未使用root权限执行！"
    exit 0
fi

file_path="./virtual_interface.csv"  # 虚拟接口配置文件
log_path="./log/"  # 日志目录

# 检测文件是否存在且不为空，不符合则退出
if [ -s ${file_path} ]
then
    # 读取文件已有行数
    int=$(awk 'END {print NR}' ${file_path})
    echo "目前已开启接口数："${int}
else
    echo "文件不存在或为空"
    exit 1
fi
# 获取用户指定删除接口数量
echo -n "请输入您想要删除的虚拟接口数量："
read num
if ((${num} <= 0))
then
    echo "错误！选择接口数量需大于0！"
    exit 2
elif ((${num} > ${int}))
then
    echo "错误！选择接口数量超出上限！"
    exit 3
else
    i=1
    while(( ${i} <= ${num} ))
    do
        # 获取用户指定删除虚拟接口名称和进程号
        tun_name=$(awk -F, 'NR=='${int}' {print $1}' ${file_path})
        pid_num=$(awk -F, 'NR=='${int}' {print $2}' ${file_path})
        # 结束VPN进程
        kill ${pid_num}
        # 删除虚拟接口
        ip tuntap del dev ${tun_name} mode tun
        # 删除文件行
        sed -i ''${int}'d' ${file_path}
        # 日志文件
        log_time=$(date "+%Y-%m-%d %H:%M:%S")
        log_name=${log_path}${tun_name}".txt"
        echo ${log_time}"  TUN接口关闭" >> ${log_name}
        echo "--------------------" >> ${log_name}
        # 从后向前删除
        let "i++"
        let "int--"
    done
fi
echo "您已成功删除虚拟接口数："${num}
echo "已开启虚拟接口数："${int}