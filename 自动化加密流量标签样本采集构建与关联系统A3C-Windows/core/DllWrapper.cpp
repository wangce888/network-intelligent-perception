#include "a3c_capture.h"
#include <locale.h>

CaptureHelper capHlp = CaptureHelper();
int t = 1;

extern "C" __declspec(dllexport) DWORD Init()
{
	return capHlp.Init();
}

extern "C" __declspec(dllexport) DWORD GetAdapterCnt()
{
	return capHlp.adpCnt;
}

extern "C" __declspec(dllexport) DWORD SelectAdapter()
{
	return capHlp.SelectAdapter();
}

extern "C" __declspec(dllexport) PWCHAR GetAdapterItem(int index)
{
	return capHlp.adapterList[index].friendlyName;
}

extern "C" __declspec(dllexport) DWORD StartCapture(int index)
{
	capHlp.selAdapter = capHlp.adapterList[index];
	return capHlp.StartCapture(capHlp.adapterList[index].adapterName);
}

extern "C" __declspec(dllexport) DWORD BreakLoop()
{
	capHlp.BreakLoop();
	return 0;
}

extern "C" __declspec(dllexport) DWORD CleanUp()
{
	return capHlp.CleanUp();
}

extern "C" __declspec(dllexport) PCHAR GetOutputDir()
{
	return _strdup(capHlp.dirName.c_str());
}

//extern "C" __declspec(dllexport) void SetPInvoke(PInvokeType fun)
//{
//	capHlp.outputFun = fun;
//}

extern "C" __declspec(dllexport) int GetPktCnt()
{
	return capHlp.pkt_cnt;
}

extern "C" __declspec(dllexport) int GetProCnt()
{
	return capHlp.pro_cnt;
}

extern "C" __declspec(dllexport) int GetCapInfo(int index, char* proName, int* cnt)
{
	if (index >= capHlp.pro_to_cnt.size())
	{
		return -1;
	}
	string key = capHlp.proIndex[index];
	*cnt = capHlp.pro_to_cnt[key];
	sprintf_s(proName, 2560, key.c_str());
	return 0;
}