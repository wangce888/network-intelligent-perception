#include "a3c_log.h"

FILE* OpenLogFile(const char* filename, const char* mod)
{
	FILE* f = fopen(filename, mod);
	return f;
}

int WriteLog(FILE* logFile, const char* format, ...)
{
	va_list arg;
	va_start(arg, format);
	int status;

	char ts_buf[32];
	time_t log_ts = time(nullptr);
	strftime(ts_buf, sizeof(ts_buf), "%Y-%m-%d %H:%M:%S", localtime(&log_ts));

	fprintf(logFile, "[%s] ", ts_buf);
	status = vfprintf(logFile, format, arg);
	va_end(arg);

	fflush(logFile);
	return status;
}

int CloseLogFile(FILE* logFile)
{
	return fclose(logFile);
}