#include "a3c_netInfo.h"
#include "a3c_capture.h"
#include "a3c_log.h"
#include <pcap.h>
#include <unordered_map>
#include <string>
#include <tchar.h>
#include <time.h>

#pragma comment(lib, "ws2_32.lib")	//用于ntohs函数的使用
#pragma comment(lib, "wpcap.lib")
#pragma comment(lib, "Packet.lib")

using namespace std;

unordered_map<u_short, string> tcp_to_pro;
unordered_map<u_short, string> udp_to_pro;
unordered_map<string, u_char*> pro_to_dumper;

pcap_t* adhandle = NULL;
pcap_dumper_t* dumpfile_all;		//存储所有数据包
pcap_dumper_t* dumpfile_unknown;	//存储未知进程数据包
pcap_dumper_t* dumpfile_other;		//存储其他数据包
pcap_dumper_t* dumpfile_dns;        //存储所有DNS

int test_cnt = 0;

void packet_handler(u_char* param, const struct pcap_pkthdr* header, const u_char* pkt_data);
void GetTransportInfo(const u_char* pkt_data, u_short* port, u_short*, u_short*);

FILE* logFile;
string dir_name;


int main(int argc, char** argv)
{
	time_t t = time(NULL);
	char dir[64] = { 0 };
	strftime(dir, sizeof(dir), "%Y-%m-%d-%H-%M-%S", localtime(&t)); //年-月-日 时-分-秒
	dir_name = dir;
	string cmd = "mkdir " + dir_name;
	system(cmd.c_str());
	
	logFile = OpenLogFile(strcat(dir, "/log.txt"), "w");

	pcap_if_t* alldevs;
	pcap_if_t* d;
	int inum;
	int status = 0;
	int i = 0;
	
	char errbuf[PCAP_ERRBUF_SIZE];
	
	/*if (!LoadNpcapDlls())
	{
		fprintf(stderr, "Couldn't load Npcap\n");
		exit(1);
	}*/

	if (pcap_findalldevs(&alldevs, errbuf) == -1)
	{
		fprintf(stderr, "Error in pcap_findalldevs: %s\n", errbuf);
		exit(1);
	}

	for (d = alldevs; d; d = d->next)
	{
		printf("%d. %s", ++i, d->name);
		if (d->description)
			printf(" (%s)\n", d->description);
		else
			printf(" (No description available)\n");
	}

	if (i == 0)
	{
		printf("\nNo interfaces found! Make sure Npcap is installed.\n");
		return -1;
	}

	printf("Enter the interface number (1-%d):", i);
	scanf_s("%d", &inum);

	if (inum < 1 || inum > i)
	{
		printf("\nInterface number out of range.\n");
		pcap_freealldevs(alldevs);
		return -1;
	}

	for (d = alldevs, i = 0; i < inum - 1 && d; d = d->next, i++);

	if (d == NULL)
	{
		printf("Select interface failed\n");
		return -1;
	}
	
	
	adhandle = pcap_create(d->name, errbuf);
	if (adhandle == NULL)
	{
		printf("Unable to open the adapter. %s is not supported by Npcap\n", d->name);
		pcap_freealldevs(alldevs);
		return -1;
	}
	
	status = pcap_set_buffer_size(adhandle, 200 * 1024 * 1024);
	if (status != 0)
	{
		printf("pcap set buffer size error\n");
		return -1;
	}
	status = pcap_set_timeout(adhandle, 100);
	if (status != 0)
	{
		printf("pcap set timeout error\n");
		return -1;
	}
	

	status = pcap_activate(adhandle);
	if (status != 0)
	{
		printf("pcap activate error\n");
		return -1;
	}

	dumpfile_all = pcap_dump_open(adhandle, (dir_name + "/all.pcap").c_str());
	dumpfile_unknown = pcap_dump_open(adhandle, (dir_name + "/unknown.pcap").c_str());
	dumpfile_other = pcap_dump_open(adhandle, (dir_name + "/other.pcap").c_str());
	dumpfile_dns = pcap_dump_open(adhandle, (dir_name + "/DNSPro.pcap").c_str());

	if (dumpfile_all == NULL || dumpfile_unknown == NULL || dumpfile_other == NULL || dumpfile_dns == NULL)
	{
		fprintf(stderr, "\nError opening output file\n");
		return -1;
	}

	printf("\nlistening on %s... Press Ctrl+C to stop...\n", d->description);

	pcap_freealldevs(alldevs);

	pcap_loop(adhandle, 0, packet_handler, (unsigned char*)dumpfile_all);

	pcap_close(adhandle);
	return 0;
}



void packet_handler(u_char* dumpfile, const struct pcap_pkthdr* header, const u_char* pkt_data)
{
	//printf("pkt cnt: %d\n", ++test_cnt);
	pcap_dump(dumpfile, header, pkt_data);

	struct tm ltime;
	char timestr[16];
	time_t local_tv_sec;
	u_short port = 0;
	u_short ip_protocol = 0;
	u_short trans_protocol = 0;
	DWORD pid = 0;

	GetTransportInfo(pkt_data, &port, &ip_protocol, &trans_protocol);

	if (trans_protocol == TYPE_TCP)
	{
		unordered_map<u_short, string>::iterator pro_it = tcp_to_pro.find(port);
		if (pro_it != tcp_to_pro.end())
		{
			u_char* dumper = pro_to_dumper[pro_it->second];
			pcap_dump(dumper, header, pkt_data);
			WriteLog(logFile, "pcap_file 1:\n");
			WriteLog(logFile, "%s\n", tcp_to_pro[port].c_str());
			WriteLog(logFile, "tcp port: %d\n", port);
			return;
		}

		if (ip_protocol == TYPE_IP)
		{
			pid = GetPidFromTcp(port);
		}
		else //IPv6
		{
			pid = GetPidFromTcp6(port);
		}

		if (pid == 0) //实际上这就是暂时不能确定的进程，先加入到unknow里，后面离线进一步关联
		{
			pcap_dump((u_char*)dumpfile_unknown, header, pkt_data);
			WriteLog(logFile, "pcap_file pid=0:\n");
			WriteLog(logFile, "unknown\n");
			WriteLog(logFile, "tcp port: %d\n", port);
			return;
		}

		string process_name = GetProcessNameByPid(pid);
		if (process_name.empty())
		{
			//printf("Get process name error\n");
			WriteLog(logFile, "empty !!!!!!!!!!!!!!!!!!!!!!!!\n");
			return;
		}
		
		process_name += ".pcap";

		unordered_map<string, u_char*>::iterator dumper_it = pro_to_dumper.find(process_name);
		if (dumper_it != pro_to_dumper.end())
		{
			pcap_dump(dumper_it->second, header, pkt_data);
			pair<u_short, string>tmp(port, process_name);
			tcp_to_pro.insert(tmp);
			WriteLog(logFile, "pcap_file 2 PID:%d\n", pid);
			WriteLog(logFile, "%s\n", tcp_to_pro[port].c_str());
			WriteLog(logFile, "tcp port: %d\n", port);
			return;
		}

		pcap_dumper_t* df = pcap_dump_open(adhandle, (dir_name + "/" + process_name).c_str());

		pcap_dump((u_char*)df, header, pkt_data);
		pair<u_short, string> tmp1(port, process_name);
		tcp_to_pro.insert(tmp1);
		pair<string, u_char*> tmp2(process_name, (u_char*)df);
		pro_to_dumper.insert(tmp2);

		WriteLog(logFile, "pcap_file 3:\n");
		WriteLog(logFile, "%s\n", tcp_to_pro[port].c_str());
		WriteLog(logFile, "tcp port: %d\n", port);
	}
	else if (trans_protocol == TYPE_UDP)//UDP
	{
		unordered_map<u_short, string>::iterator pro_it = udp_to_pro.find(port);
		if (pro_it != udp_to_pro.end())
		{
			u_char* dumper = pro_to_dumper[pro_it->second];
			pcap_dump(dumper, header, pkt_data);
			WriteLog(logFile, "pcap_file 1:\n");
			WriteLog(logFile, "%s\n", udp_to_pro[port].c_str());
			WriteLog(logFile, "udp port: %d\n", port);
			return;
		}

		if (ip_protocol == TYPE_IP)
		{
			pid = GetPidFromUdp(port);
		}
		else //IPv6
		{
			pid = GetPidFromUdp6(port);
		}
		//UDP多了这一步，一个端口被多个进程使用时也在这里处理
		if (pid == UNKNOWN_PID)
		{
			pcap_dump((u_char*)dumpfile_other, header, pkt_data);
			WriteLog(logFile, "pcap_file:\n");
			WriteLog(logFile, "other\n");
			WriteLog(logFile, "udp port: %d\n", port);
			return;
		}

		if (pid == 0) //实际上这就是暂时不能确定的进程，先加入到unknow里，后面离线进一步关联
		{
			pcap_dump((u_char*)dumpfile_unknown, header, pkt_data);
			WriteLog(logFile, "pcap_file pid=0:\n");
			WriteLog(logFile, "unknown\n");
			WriteLog(logFile, "udp port: %d\n", port);
			return;
		}

		string process_name = GetProcessNameByPid(pid);
		if (process_name.empty())
		{
			//printf("Get process name error\n");
			WriteLog(logFile, "empty !!!!!!!!!!!!!!!!!!!!!!!\n");
			return;
		}
		
		process_name += ".pcap";
		unordered_map<string, u_char*>::iterator dumper_it = pro_to_dumper.find(process_name);
		if (dumper_it != pro_to_dumper.end())
		{
			pcap_dump(dumper_it->second, header, pkt_data);
			pair<u_short, string>tmp(port, process_name);
			udp_to_pro.insert(tmp);

			WriteLog(logFile, "pcap_file 2 PID: %d\n", pid);
			WriteLog(logFile, "%s\n", udp_to_pro[port].c_str());
			WriteLog(logFile, "udp port: %d\n", port);
			return;
		}

		pcap_dumper_t* df = pcap_dump_open(adhandle, (dir_name + "/" + process_name).c_str());
		
		pcap_dump((u_char*)df, header, pkt_data);
		pair<u_short, string> tmp1(port, process_name);
		udp_to_pro.insert(tmp1);
		pair<string, u_char*> tmp2(process_name, (u_char*)df);
		pro_to_dumper.insert(tmp2);

		WriteLog(logFile, "pcap_file 3:\n");
		WriteLog(logFile, "%s\n", udp_to_pro[port].c_str());
		WriteLog(logFile, "udp port: %d\n", port);
	}
	else if (trans_protocol == TYPE_DNS)
	{
		pcap_dump((u_char*)dumpfile_dns, header, pkt_data);
		WriteLog(logFile, "pcap_file:\n");
		WriteLog(logFile, "DNSPro\n");
		WriteLog(logFile, "port: %d\n", port);
	}
	else
	{
		pcap_dump((u_char*)dumpfile_other, header, pkt_data);
		WriteLog(logFile, "pcap_file:\n");
		WriteLog(logFile, "other\n");
		WriteLog(logFile, "port: %d\n", port);
		return;
	}
}

void GetTransportInfo(const u_char* pkt_data, u_short* port, u_short* ip_protocol, u_short* trans_protocol)
{
	u_short src_port;
	u_short dst_port;
	EthHeader* eh = NULL;
	eh = (EthHeader*)pkt_data;
	*ip_protocol = eh->type;

	if (eh->type == TYPE_IP)
	{
		IPHeader *ih = NULL;
		ih = (IPHeader*)(pkt_data + 14);
		*trans_protocol = ih->protocol;
		if (ih->protocol == TYPE_TCP)
		{
			TcpHeader* th = NULL;
			th = (TcpHeader*)((u_char*)ih + (ih->ver_hdlen & 0xF) * 4);
			src_port = ntohs(th->src_port);
			dst_port = ntohs(th->dst_port);
			*port = src_port > dst_port ? src_port:dst_port;
		}
		else if (ih->protocol == TYPE_UDP)
		{
			UdpHeader* uh = NULL;
			uh = (UdpHeader*)((u_char*)ih + (ih->ver_hdlen & 0xF) * 4);
			src_port = ntohs(uh->src_port);
			dst_port = ntohs(uh->dst_port);
			*port = src_port > dst_port ? src_port : dst_port;
			if (src_port == 53 || dst_port == 53)	//针对DNS的特殊处理
			{
				*trans_protocol = TYPE_DNS;
			}
		}
		else
		{
			//printf("Unknown protocol at transport layer\n");
		}
	}
	else if (eh->type == TYPE_IP6)
	{
		IPv6Header* ih = NULL;
		ih = (IPv6Header*)(pkt_data + 14);
		*trans_protocol = ih->next_header;
		if (ih->next_header == TYPE_TCP)
		{
			TcpHeader* th = NULL;
			th = (TcpHeader*)((u_char*)ih + 40);
			src_port = ntohs(th->src_port);
			dst_port = ntohs(th->dst_port);
			*port = src_port > dst_port ? src_port : dst_port;
		}
		else if (ih->next_header == TYPE_UDP)
		{
			UdpHeader* uh = NULL;
			uh = (UdpHeader*)((u_char*)ih + 40);
			src_port = ntohs(uh->src_port);
			dst_port = ntohs(uh->dst_port);
			*port = src_port > dst_port ? src_port : dst_port;
			if (src_port == 53 || dst_port == 53)	//针对DNS的特殊处理
			{
				*trans_protocol = TYPE_DNS;
			}
		}
		else
		{
			//printf("Unknown protocol at transport layer\n");
		}
	}
	else
	{
		//printf("Unknown protocol at ip layer\n");
	}
}


