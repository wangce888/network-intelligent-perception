import win32gui     # pip install pywin32
import win32con
import win32api
import win32clipboard as clipboard
import pythoncom
import time
import win32com
from PIL import Image          # pip install PILLOW
from win32com.client import Dispatch
from io import BytesIO
#from apscheduler.schedulers.blocking import BlockingScheduler
#from urllib import request
"""
此程序可以操作windows平台的桌面软件。但是注意：运行代码时桌面软件不可最小化，可以移到一旁，最小化发送失败。
"""
class Win_Soft(object):
    def __init__(self, torrentFile):                  # magnet字符串
        #self.className = 'Chrome_WidgetWin_0'    # 迅雷的类名，可通过 spy++ 软件查看
        self.className = 'BT4823DF041B09'         # BitTorrent className
        self.torrent = torrentFile

    """
        ctrLC:模拟 Ctrl+C
    """
    def ctrlC(self):
        clipboard.OpenClipboard()
        clipboard.EmptyClipboard()
        clipboard.SetClipboardData(win32con.CF_UNICODETEXT, self.torrent)
        clipboard.CloseClipboard()
        return

    """
    ctrLV:模拟 Ctrl+V
    """
    def ctrlV(self):
        win32api.keybd_event(17, 0, 0, 0)  # ctrl
        win32api.keybd_event(86, 0, 0, 0)  # V
        win32api.keybd_event(86, 0, win32con.KEYEVENTF_KEYUP, 0)  # 释放按键
        win32api.keybd_event(17, 0, win32con.KEYEVENTF_KEYUP, 0)

    """
    click_left: 模拟左点击过程
    """
    def click_left(self):
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0, 0, 0)

    """
    click_right: 模拟右点击过程
    """
    def click_right(self):
        win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0)
        win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0)

    """
    movePos:移动鼠标的位置
    """
    def movePos(self, x, y):
        win32api.SetCursorPos((x, y))

    """
    enter:模拟enter
    """
    def enter(self):
        win32api.keybd_event(13, 0, 0, 0)
        win32api.keybd_event(13, 0, win32con.KEYEVENTF_KEYUP, 0)

    def get_window(self):
        win = win32gui.FindWindow(self.className, "BitTorrent 7.10.5  (build 46097) [32-bit]")  # param1需要传入窗口的类名，param2需要传入窗口的标题
        win32gui.ShowWindow(win, win32con.SW_SHOW)
        # print("找到句柄：%x" % win)
        # left, top, right, bottom = win32gui.GetWindowRect(win)
        # print(left, top, right, bottom)  # 最小化为负数
        pythoncom.CoInitialize()
        shell = win32com.client.Dispatch("WScript.Shell")
        shell.SendKeys('%')
        win32gui.SetForegroundWindow(win)  # 获取控制
        win32gui.MoveWindow(win, 0, 0, 1100, 600, True)
        time.sleep(1)
        return

    # 操作bittorrent，下载种子
    def manipulate_bittorrent(self):
        self.get_window()
        print("找到Bittorrent窗口！")
        time.sleep(1)
        self.movePos(330, 165)
        self.ctrlC()
        self.click_left()
        time.sleep(1)
        self.ctrlV()
        time.sleep(1)
        self.movePos(470, 500)
        self.click_left()
        time.sleep(1)
        self.movePos(350, 195)
        self.click_left()
        time.sleep(1)
        self.movePos(470, 500)
        self.click_left()
        time.sleep(1)
        # self.movePos(880, 620)
        # self.click_left()
        # time.sleep(1)


    # 删除下载中的任务
    def delete_task(self):
        self.movePos(600, 220)
        self.click_right()
        time.sleep(1)
        self.movePos(700, 560)
        time.sleep(1)
        self.movePos(900, 600)
        time.sleep(1)
        self.click_left()
        time.sleep(1)
        # 按Y键 89
        win32api.keybd_event(89, 0, 0, 0)  # Y
        win32api.keybd_event(89, 0, win32con.KEYEVENTF_KEYUP, 0)  # 释放按键



# for test
if __name__ == '__main__':
    obj = Win_Soft('D:\\Ray_Xiao_c\\torrent\\20')
    obj.manipulate_bittorrent()
    obj.delete_task()
