# Tor流量生成脚本


主要功能：生成Tor流量并在本机、网关和服务端收集

## 配置Tor环境
以windows为例，通过firefox->proxifier->vidalia->Tor实现firefox访问Tor

首先下载Tor程序和Vidalia。Vidalia是Tor开发者发布的Tor跨平台GUI控制工具。使用Vidalia可以方便快捷地配置Tor。这里在Windows端使用Vidalia配置Tor客户端。启动后打开设置，在“常规”中指定Tor运行路径，在“高级”选项中选择Tor配置文件（Torrc，参考中继节点）和数据目录（log），点击“启动Tor”，不一会儿就可以连接到所搭建的私有Tor网络上。从图1-11控制面板可以查看Tor网络状态，检查log日志，Tor网络性能等，在Tor网络地图中可以看到该小型网络中的所有节点状态。

<figure class="half">
    <img src="image/vidalia.png" width="294"/>
    <img src="image/Tor网络地图.png" width="377"/>
</figure>

Vidalia成功连接Tor网络后，会根据Torrc配置在本地9050端口开启Socks5代理。任何从9050端口接收的流量都将使用Tor协议加密转发。然而，socks代理并不能直接对HTTP代理，因此此时浏览器无法使用Tor。我们用使用Privoxy 将 HTTP代理到Socks5。Privoxy是一款针对HTTP、HTTPS协议的带过滤功能的代理服务器。

![代理过程](image/代理过程.png)

下载安装Privoxy，在Privoxy中设置所有网络由本地9050端口的Socks5协议代理。

```
Forward-Socks5t /   127.0.0.1:9050 .
```

然后设置Windows系统代理，Privoxy默认代理端口为8118

代理设置成功后打开浏览器浏览网页成功，表明Windows端成功使用Tor网络。

## 生成流量
包含Audio,Browser,Mail,Message,P2P,VoIP,Video七种流量生成，具体使用请点击对应README
