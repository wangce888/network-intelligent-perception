#pragma once

#define INPUT_PARAMS				// ????????????????????????params.h?е???????????????????

#ifndef INPUT_PARAMS

//#define DATASET_PATH				"D:\\???\\???\\20221101 TopK\\?????\\chn1"
//#define DATASET_PATH				"D:\\???\\???\\20221101 TopK\\?????\\CAIDA2018\\My130000.dat"
//#define DATASET_PATH				"D:\\???\\???\\20221101 TopK\\?????\\campus\\campus.dat"

#define TOP_K						100
#define PACKET_NUM					10000000
#define FLOW_NUM					PACKET_NUM

#define COUNTER_NUM					131072 - TOP_K * 13 * 8 * 2	// 2 ^ 17
#define DEPTH						3
#define WITCH						(COUNTER_NUM / DEPTH)
#define L							88
#define SAMPLING_PROBABILITY_1		(L - ((COUNTER_THRESHOLDS_1) -1))
#define SAMPLING_PROBABILITY_2		(SAMPLING_PROBABILITY_1 + 64)
#define SAMPLING_PROBABILITY_3		(SAMPLING_PROBABILITY_2 + 128)
#endif


//#define L							88
//#define DATASET_PATH				"M:\\2022实验\\Data\\CAIDA2018.dat"
//#define CERNET30				"M:\\2022实验\\Data\\CERNET30"
//#define CERNET60				"M:\\2022实验\\Data\\CERNET60"
#define COUNTER_THRESHOLDS_1		63							// 2 ^ 6 - 1
#define COUNTER_THRESHOLDS_2		127							// 2 ^ 7 - 1
#define COUNTER_THRESHOLDS_3		255							// 2 ^ 8 - 1

//#define SAMPLING_PROBABILITY_1		(L - ((COUNTER_THRESHOLDS_1) -1))
//#define SAMPLING_PROBABILITY_2		(SAMPLING_PROBABILITY_1 + 64)
//#define SAMPLING_PROBABILITY_3		(SAMPLING_PROBABILITY_2 + 128)


#define byte						unsigned char